//
//  Constants.swift
//  ProjectStructure
//
//  Created by Calista Bertha on 6/16/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation

enum BuildType {
    case development
    case stagging
    case production
}

struct Config {
    static let appKey = "a5120ae232de05c4b3b54a1a8a8bfd17"
    
    /*
     *  Define URL of REST API
     */
    
    static let baseAPI: BuildType = .stagging
    
    static let developmentBaseAPI = "https://www.truvel.com/api/Mobile_API/"
    static let staggingBaseAPI = "http://staging.truvel.com/api/Mobile_API/"
    static let productionBaseAPI = "https://www.truvel.com/api/Mobile_API/"
    
    //"https://www.truvel.com/api/Mobile_API/"
    //"http://staging.truvel.com/api/Mobile_API/"
    
    /*
     *  Define base URL of picture
     */
    static let basePictureAPI = "https://image.tmdb.org/t/p/w500"
}

struct REST {
    struct Movie {
        static let popular = "movie/popular"
        static let topRated = "movie/top_rated"
    }
    
    struct ServiceAPI {
        struct Airport {
            static let listAirport = "GetAirports"
            static let airAvailable = "GetAirAvailability"
            static let filter = "https://www.truvel.com/truvel_api/flight/api_filter"
        }
        
        struct Booking {
            static let bookFlight = "BookFlight"
        }
        
        // Agung Code
        struct Invoice {
            static let doInvoiceData = "DoInvoiceData"
        }
        
        struct Payment {
            static let cc = "http://staging.truvel.com/payment_flight/payment/paymentDoku/genpaycodecc.php"
            static let mc = "http://staging.truvel.com/payment_flight/payment/paymentDoku/mandiri.php"
            static let va = "http://staging.truvel.com/payment_flight/payment/paymentDoku/genpaycodeva.php"
        }
    }
    
    struct StoryboardReferences {
        static let main = "Main"
        static let authentication = "Authentication"
        static let account = "Account"
        static let flight = "Flight"
        
    }
    
    struct ViewControllerID {
        struct Main {
            static let splash = "SplashViewController"
            static let home = "HomeViewController"
            static let menu = "MenuBarViewController"
        }
        
        struct Flight {
            static let search = "SearchFlightViewController"
            static let flying = "FlyingSearchViewController"
            static let calender = "CalenderViewController"
            static let passenger = "ChangePassengerViewController"
            static let result = "ResultFlightViewController"
            static let filter = "FilterFlightViewController"
            static let contactInfo = "ContactInformationViewController"
            static let sort = "SortFlightViewController"
            static let summary = "FlightSummaryViewController"
            static let contactDetail = "ContactDetailViewController"
            static let warning = "WarningViewController"
            static let review = "ReviewBookingViewController"
            static let payment = "PaymentViewController"
            static let success = "SuccessPaymentViewController"
        }
    }
}


