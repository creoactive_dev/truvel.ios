//
//  DateHelper.swift
//  ZULU
//
//  Created by Calista on 9/28/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation

protocol DateHelperProtocol {}
extension DateHelperProtocol {
    static func dateFormat(dateString: String, withFormat format: FormatDate) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        return dateFormatter.date(from: dateString) ?? Date()
    }
    
    static func stringFormat(date: Date, withFormat format: FormatDate) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        
        return dateFormatter.string(from: date)
    }
    
}

enum FormatDate: String {
    case yyyyMMdd = "yyyy-MM-dd"
    case MMMMddyyyy = "MMMM dd, yyyy"
}

class DateHelper: DateHelperProtocol {
    static func weeklyDayRecap() -> String?{
        let calendar = Calendar(identifier: .gregorian)
        let today = Date()
        guard let lastWeek = calendar.date(byAdding: .day, value: -7, to: today, wrappingComponents: false) else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        let lastWeekDate = dateFormatter.string(from: lastWeek)
        
        dateFormatter.dateFormat = "dd MMMM yyyy"
        let todayDate = dateFormatter.string(from: today)
        
        return "\(lastWeekDate) - \(todayDate)"
    }
    
    static func humanableFormattedDate(date: Date){

    }
}
