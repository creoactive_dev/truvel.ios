//
//  AirAvailable.swift
//  Truvel
//
//  Created by Calista on 11/1/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class AirAvailable {
    var flightJourneys: [FlightJourneys]
    var arrivalAirport: ListAirport
    var departureAirport: ListAirport
    
    init(flightJourneys: [FlightJourneys],arrivalAirport:ListAirport, departureAirport:ListAirport) {
        self.flightJourneys = flightJourneys
        self.arrivalAirport = arrivalAirport
        self.departureAirport = departureAirport
    }
    
    convenience init(json: JSON){
        var flights = [FlightJourneys]()
        for value in json["flightJourneys"].arrayValue {
            let flight = FlightJourneys(json: value)
            flights.append(flight)
        }
        
        let departure = ListAirport(json: json["departureAirport"])
        let arrival = ListAirport(json:json["arrivalAirport"])
        self.init(flightJourneys: flights, arrivalAirport: arrival, departureAirport: departure)
    }
}
