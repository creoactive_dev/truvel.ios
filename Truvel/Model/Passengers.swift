//
//  Passengers.swift
//  Truvel
//
//  Created by Calista on 11/20/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import SwiftyJSON
class Passengers {
    var passNum : Int
    var idNo: String
    var paxType : String
    var fName : String
    var mName : String
    var lName : String
    var title : String
    var dob : String
    var gender : String
    var nationality : String
    var ticketNum : String
    var paxFare : Fares
    var infant : String
    var passport : Passport?
    
    init( passNum : Int, idNo: String, paxType : String, fName : String, mName : String, lName : String,title : String, dob : String, gender : String, nationality : String, ticketNum : String, paxFare : Fares, infant : String, passport : Passport? = nil) {
        self.passNum = passNum
        self.idNo = idNo
        self.paxType = paxType
        self.fName = fName
        self.mName = mName
        self.lName = lName
        self.title = title
        self.dob = dob
        self.gender = gender
        self.nationality = nationality
        self.ticketNum = ticketNum
        self.paxFare = paxFare
        self.infant = infant
        self.passport = passport
    }
    
    convenience init(json: JSON){
        let passNum = json["passengerNumber"].intValue
        let idNo = json["idNo"].stringValue
        let paxType = json["paxType"].stringValue
        let fName = json["firstName"].stringValue
        let mName = json["middleName"].stringValue
        let lName = json["lastName"].stringValue
        let title = json["title"].stringValue
        let dob = json["dob"].stringValue
        let gender = json["gender"].stringValue
        let nationality = json["nationality"].stringValue
        let ticketNum = json["ticketNumber"].stringValue
        let paxFare = Fares(json:json["paxFare"])
        let infant = json["infant"].stringValue
        let pasport = Passport(json:json["Passport"])

        self.init(passNum: passNum, idNo: idNo, paxType: paxType, fName: fName, mName: mName, lName: lName, title: title, dob: dob, gender: gender, nationality: nationality, ticketNum: ticketNum, paxFare: paxFare, infant: infant, passport: pasport)
    }
 
}

//class PaxFare {
//    var fareSellKey : String
//    var fareType : String
//    var classOfService : String
//    var currency : String
//    var basicFare : Int
//    var basicVat : Int
//    var iwjr : Int
//    var airportTax : Int
//    var fuelSurcharge : Int
//    var adminFee : Int
//    var baggageAllowance : String
//    var insurance : String
//    var otherFee : Int
//    
//    init(fareSellKey : String, fareType : String, classOfService : String, currency : String, basicFare : Int, basicVat : Int, iwjr : Int, airportTax : Int, fuelSurcharge : Int, adminFee : Int, baggageAllowance : String, insurance : String, otherFee : Int) {
//        self.fareSellKey = fareSellKey
//        self.fareType = fareType
//        self.classOfService = classOfService
//        self.currency = currency
//        self.basicFare = basicFare
//        self.basicVat = basicVat
//        self.iwjr = iwjr
//        self.airportTax = airportTax
//        self.fuelSurcharge = fuelSurcharge
//        self.adminFee = adminFee
//        self.baggageAllowance = baggageAllowance
//        self.insurance = insurance
//        self.otherFee = otherFee
//    }
//    
//    convenience init(json: JSON){
//        let fareSellKey = json["fareSellKey"].stringValue
//        let fareType = json["fareType"].stringValue
//        let classOfService = json["classOfService"].stringValue
//        let currency = json["currency"].stringValue
//        let basicFare = json["basicFare"].intValue
//        let basicVat = json["paxType"].intValue
//        let iwjr = json["basicVat"].intValue
//        let airportTax = json["airportTax"].intValue
//        let fuelSurcharge = json["fuelSurcharge"].intValue
//        let adminFee = json["adminFee"].intValue
//        let baggageAllowance = json["baggageAllowance"].stringValue
//        let insurance = json["insurance"].stringValue
//        let otherFee = json["otherFee"].intValue
//        
//        self.init(fareSellKey: fareSellKey, fareType: fareType, classOfService: classOfService, currency: currency, basicFare: basicFare, basicVat: basicVat, iwjr: iwjr, airportTax: airportTax, fuelSurcharge: fuelSurcharge, adminFee: adminFee, baggageAllowance: baggageAllowance, insurance: insurance, otherFee: otherFee)
//    }
//}

class Passport {
    var passNum : String
    var dob : String
    var issuerCountry : String
    var passportExpired : String
    var nationality : String
    
    init(passNum : String, dob : String,issuerCountry : String, passportExpired : String, nationality : String) {
        self.passNum = passNum
        self.dob = dob
        self.issuerCountry = issuerCountry
        self.passportExpired = passportExpired
        self.nationality = nationality
    }
    
    convenience init(json: JSON){
        let passNum = json["passportNumber"].stringValue
        let dob = json["dob"].stringValue
        let issuerCountry = json["issuerCountry"].stringValue
        let passportExpired = json["passportExpiryDate"].stringValue
        let nationality = json["nationality"].stringValue
        
        self.init(passNum: passNum, dob: dob, issuerCountry: issuerCountry, passportExpired: passportExpired, nationality: nationality)
    }
}
