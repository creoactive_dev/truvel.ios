//
//  Fares.swift
//  Truvel
//
//  Created by Calista on 11/1/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class Fares {
    var fareType : String
    var serviceFee: Int
    var basicVat : Int
    var airportTax :Int
    var adminFee : Int
    var otherFee : Int
    var childVat : Int
    var currency : String
    var fuelSurcharge : Int
    var discount : Int
    var infantVat : Int
    var baggageAllowance : String
    var fareSellKey : String
    var availableCount : Int
    var insurance : String
    var iwjr: Int
    var uid: String
    var OtherFare: String
    var cabin : String
    var classOfService : String
    var basicFare: Int
    var childFare : Int
    var infantFare : Int
    
    init(fareType : String, serviceFee: Int, basicVat : Int, airportTax :Int, adminFee : Int, otherFee : Int, childVat : Int, currency : String, fuelSurcharge : Int, discount : Int, infantVat : Int, baggageAllowance : String, fareSellKey : String, availableCount : Int, insurance : String, iwjr: Int, uid: String, OtherFare: String, cabin : String, classOfService : String, basicFare: Int, childFare : Int, infantFare : Int) {
        self.fareType = fareType
        self.serviceFee = serviceFee
        self.basicVat = basicVat
        self.airportTax = airportTax
        self.adminFee = adminFee
        self.otherFee = otherFee
        self.childVat = childVat
        self.currency = currency
        self.fuelSurcharge = fuelSurcharge
        self.discount = discount
        self.infantVat = infantVat
        self.baggageAllowance = baggageAllowance
        self.fareSellKey = fareSellKey
        self.availableCount = availableCount
        self.insurance = insurance
        self.iwjr = iwjr
        self.uid = uid
        self.OtherFare = OtherFare
        self.cabin = cabin
        self.classOfService = classOfService
        self.basicFare = basicFare
        self.childFare = childFare
        self.infantFare = infantFare
    }
    
    convenience init(json: JSON){
        let fareType = json["fareType"].stringValue
        let serviceFee = json["serviceFee"].intValue
        let basicVat = json["basicVat"].intValue
        let airportTax = json["airportTax"].intValue
        let adminFee = json["adminFee"].intValue
        let otherFee = json["otherFee"].intValue
        let childVat = json["childVat"].intValue
        let currency = json["currency"].stringValue
        let fuelSurcharge = json["fuelSurcharge"].intValue
        let discount = json["discount"].intValue
        let infantVat = json["infantVat"].intValue
        let baggageAllowance = json["baggageAllowance"].stringValue
        let fareSellKey = json["fareSellKey"].stringValue
        let availableCount = json["availableCount"].intValue
        let insurance = json["insurance"].stringValue
        let iwjr = json["iwjr"].intValue
        let uid = json["uid"].stringValue
        let OtherFare = json["OtherFare"].stringValue
        let cabin = json["cabin"].stringValue
        let classOfService = json["classOfService"].stringValue
        let basicFare = json["basicFare"].intValue
        let childFare = json["childFare"].intValue
        let infantFare = json["infantFare"].intValue
        
        self.init(fareType: fareType, serviceFee: serviceFee, basicVat: basicVat, airportTax: airportTax, adminFee: adminFee, otherFee: otherFee, childVat: childVat, currency: currency, fuelSurcharge: fuelSurcharge, discount: discount, infantVat: infantVat, baggageAllowance: baggageAllowance, fareSellKey: fareSellKey, availableCount: availableCount, insurance: insurance, iwjr: iwjr, uid: uid, OtherFare: OtherFare, cabin: cabin, classOfService: classOfService, basicFare: basicFare, childFare: childFare, infantFare: infantFare)
    }
}
