//
//  ListAirport.swift
//  Truvel
//
//  Created by Calista on 10/31/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListAirport {
    var city: String
    var cityCode: String
    var code: String
    var countryCode: String
    var latitude: Int
    var logitude: Int
    var name : String
    var timezone: String
    
    init(city: String, cityCode: String, code: String, countryCode: String, latitude: Int, logitude: Int, name : String, timezone: String ) {
        self.city = city
        self.cityCode = cityCode
        self.code = code
        self.countryCode = countryCode
        self.latitude = latitude
        self.logitude = logitude
        self.name = name
        self.timezone = timezone
    }
    
    convenience init(json: JSON){
        let city = json["city"].stringValue
        let cityCode = json["cityCode"].stringValue
        let code = json["code"].stringValue
        let countryCode = json["countryCode"].stringValue
        let latitude = json["latitude"].intValue
        let longitude = json["longitude"].intValue
        let name = json["name"].stringValue
        let timezone = json["timezone"].stringValue
        
        self.init(city: city, cityCode: cityCode, code: code, countryCode: countryCode, latitude: latitude, logitude: longitude, name: name, timezone: timezone)
    }

}
