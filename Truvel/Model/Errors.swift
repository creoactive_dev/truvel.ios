//
//  Errors.swift
//  Truvel
//
//  Created by Calista on 10/31/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class Errors{
    var code : Int
    var message : String
    var type : String
    
    
    init(code :Int, message: String, type: String) {
        self.code = code
        self.message = message
        self.type = type
        
    }
    
    convenience init(json: JSON){
        let code = json["code"].intValue
        let message = json["message"].stringValue
        let type = json["type"].stringValue
        
        self.init(code: code, message: message, type: type)

    }
}
