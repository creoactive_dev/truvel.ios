//
//  ListInvoice.swift
//  Truvel
//
//  Created by CreoActive on 12/21/17.
//  Copyright © 2017 codigo. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListInvoice {
    var invoiceNo: String
    var localAmt: Int
    var foreignAmt: Int
    var currencyId: String
    
    init(invoiceNo: String, localAmt: Int, foreignAmt: Int, currencyId: String) {
        self.invoiceNo = invoiceNo
        self.localAmt = localAmt
        self.foreignAmt = foreignAmt
        self.currencyId = currencyId
    }
    
    convenience init(json: JSON){
        let invoiceNo = json["InvoiceNo"].stringValue
        let localAmt = json["LocalAmt"].intValue
        let foreignAmt = json["ForeignAmt"].intValue
        let currencyId = json["CurrencyCd"].stringValue
        
        self.init(invoiceNo: invoiceNo, localAmt: localAmt, foreignAmt: foreignAmt, currencyId: currencyId)
    }
    
}
