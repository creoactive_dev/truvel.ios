//
//  FlightJourneys.swift
//  Truvel
//
//  Created by Calista on 11/1/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlightJourneys {
    var departureAirport: ListAirport
    var uid : String
    var arrivalAirport : ListAirport
    var flightSegments : [FlightSegments]
    var stopCount : Int
    var journeySellKey : String
    var cabin : String
    
    init(departureAirport: ListAirport, uid : String, arrivalAirport : ListAirport, flightSegments : [FlightSegments], stopCount : Int, journeySellKey : String, cabin : String) {
        self.departureAirport = departureAirport
        self.uid = uid
        self.arrivalAirport = arrivalAirport
        self.flightSegments = flightSegments
        self.stopCount = stopCount
        self.journeySellKey = journeySellKey
        self.cabin = cabin
    }
    
    convenience init(json: JSON){
        let departure = ListAirport(json: json["departureAirport"])
        let uid = json["uid"].stringValue
        let arrival = ListAirport(json:json["arrivalAirport"])
        var flights = [FlightSegments]()
        for value in json["flightSegments"].arrayValue {
            let flight = FlightSegments(json: value)
            flights.append(flight)
        }
        let stopCount = json["stopCount"].intValue
        let journeySellKey = json["journeySellKey"].stringValue
        let cabin = json["cabin"].stringValue

        self.init(departureAirport: departure, uid: uid, arrivalAirport: arrival, flightSegments: flights, stopCount: stopCount, journeySellKey: journeySellKey, cabin: cabin)
    }
}
