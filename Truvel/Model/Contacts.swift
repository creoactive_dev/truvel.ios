//
//  Contacts.swift
//  Truvel
//
//  Created by Calista on 11/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import SwiftyJSON

class Contacts {
    var fName : String
    var mName : String
    var lName : String
    var title : String
    var homePhone : String
    var mobilePhone : String
    var workPhone : String
    var email :String
    var city : String
    var postalCode : String
    var address1 : String
    var address2 : String
    var address3 : String
    
    init(fName : String, mName : String, lName : String, title : String, homePhone : String, mobilePhone : String, workPhone : String, email :String, city : String, postalCode : String, address1 : String, address2 : String, address3 : String) {
        self.fName = fName
        self.mName = mName
        self.lName = lName
        self.title = title
        self.homePhone = homePhone
        self.mobilePhone = mobilePhone
        self.workPhone = workPhone
        self.email = email
        self.city = city
        self.postalCode = postalCode
        self.address1 = address1
        self.address2 = address2
        self.address3 = address3
    }
    
    convenience init(json: JSON){
        let fname = json["firstName"].stringValue
        let mName = json["middleName"].stringValue
        let lName = json["lastName"].stringValue
        let title = json["title"].stringValue
        let homePhone = json["homePhone"].stringValue
        let mobilePhone = json["mobilePhone"].stringValue
        let workPhone = json["workPhone"].stringValue
        let email = json["email"].stringValue
        let city = json["city"].stringValue
        let postalCode = json["postalCode"].stringValue
        let address1 = json["addressLine1"].stringValue
        let address2 = json["addressLine2"].stringValue
        let address3 = json["addressLine3"].stringValue
        
        self.init(fName: fname, mName: mName, lName: lName, title: title, homePhone: homePhone, mobilePhone: mobilePhone, workPhone: workPhone, email: email, city: city, postalCode: postalCode, address1: address1, address2: address2, address3: address3)

    }
}
