//
//  FlightSegments.swift
//  Truvel
//
//  Created by Calista on 11/1/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON

class FlightSegments {
    var id: String
    var includedBaggage : Int
    var uid : String
    var airline: Airlines
    var cabin: String
    var selectedFare: String
    var segmentSellKey: String
    var arrivalAirport: ListAirport
    var gdsCode: String
    var flightDesignator: FlightDesignator
    var departureAirport: ListAirport
    var fares: [Fares]
    var includedMeal: Int
    var sta : String
    var duration :Duration
    var std : String
    var stopCount : Int
    var aircraftCode: String
    var stopLeg : String
    
    init(id: String, includedBaggage : Int, uid : String, airline: Airlines, cabin: String, selectedFare: String, segmentSellKey: String, arrivalAirport: ListAirport, gdsCode: String, flightDesignator: FlightDesignator, departureAirport: ListAirport, fares: [Fares], includedMeal: Int, sta : String, duration: Duration, std: String, stopCount : Int, aircraftCode: String, stopLeg : String) {
        self.id = id
        self.includedBaggage = includedBaggage
        self.uid = uid
        self.airline = airline
        self.cabin = cabin
        self.selectedFare = selectedFare
        self.segmentSellKey = segmentSellKey
        self.arrivalAirport = arrivalAirport
        self.gdsCode = gdsCode
        self.flightDesignator = flightDesignator
        self.departureAirport = departureAirport
        self.fares = fares
        self.includedMeal = includedMeal
        self.sta = sta
        self.duration = duration
        self.std = std
        self.stopCount = stopCount
        self.aircraftCode = aircraftCode
        self.stopLeg = stopLeg
    }
    
    convenience init(json: JSON){
        let id = json["id"].stringValue
        let includedBaggage = json["includedBaggage"].intValue
        let uid = json["uid"].stringValue
        let cabin = json["cabin"].stringValue
        let selectedFare = json["selectedFare"].stringValue
        let segmentSellKey = json["segmentSellKey"].stringValue
        let arrivals = ListAirport(json:json["arrivalAirport"])
        let departures = ListAirport(json:json["departureAirport"])
        let airlines = Airlines(json:json["airline"])
        let durations = Duration(json:json["duration"])
        let flightDesignators = FlightDesignator(json:json["flightDesignator"])
    
        var fares = [Fares]()
        for value in json["fares"].arrayValue {
            let fare = Fares(json: value)
            fares.append(fare)
        }
        
        let gdsCode = json["GDSCode"].stringValue
        let includedMeal = json["includedMeal"].intValue
        let sta = json["sta"].stringValue
        let std = json["std"].stringValue
        let stopCount = json["stopCount"].intValue
        let aircraftCode = json["aircraftCode"].stringValue
        let stopLeg = json["stopLeg"].stringValue

        self.init(id: id, includedBaggage: includedBaggage, uid: uid, airline: airlines, cabin: cabin, selectedFare: selectedFare, segmentSellKey: segmentSellKey, arrivalAirport: arrivals, gdsCode: gdsCode, flightDesignator: flightDesignators, departureAirport: departures, fares: fares, includedMeal: includedMeal, sta: sta, duration: durations, std: std, stopCount: stopCount, aircraftCode: aircraftCode, stopLeg: stopLeg)
        
    }
    
}

class Airlines {
    var name : String
    var code : String
    
    init(name: String, code: String) {
        self.name = name
        self.code = code
    }
    
    convenience init(json: JSON){
        let name = json["name"].stringValue
        let code = json["code"].stringValue
        
        self.init(name: name, code: code)
    }
}

class FlightDesignator {
    var flightNumber: String
    var carrierCode: String
    
    init(flightNumber: String, carrierCode: String) {
        self.flightNumber = flightNumber
        self.carrierCode = carrierCode
    }
    
    convenience init(json: JSON){
        let flightNumber = json["flightNumber"].stringValue
        let carrierCode = json["carrierCode"].stringValue
        
        self.init(flightNumber: flightNumber, carrierCode: carrierCode)
    }
    
}

class Duration{
    var hours : String
    var minutes : String
    var seconds : String
    
    init(hours: String, minutes: String, seconds: String) {
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
    }
    
    convenience init(json: JSON){
        let hours = json["hours"].stringValue
        let minutes = json["minutes"].stringValue
        let seconds = json["seconds"].stringValue
        
        self.init(hours: hours, minutes: minutes, seconds: seconds)
    }
    
}
