//
//  TopFlightHomeCollectionViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class TopFlightHomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var imgFlight: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

}

extension TopFlightHomeCollectionViewCell: CollectionViewCellProtocol{
    static func configure<T>(context: UIViewController, collectionView: UICollectionView, indexPath: IndexPath, object: T) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TopFlightHomeCollectionViewCell.identifier, for: indexPath) as! TopFlightHomeCollectionViewCell
       guard let data = object as? Int else { return cell }
        if data == 0 {
            cell.lblTitle.text = "Labuan Bajo"
        }else if data == 1 {
            let dataStatic = ["Bali","Jogja","Surabaya","Medan","Lombok"]
            let dataStaticImg = ["bali.jpg","jogja.jpg","surabaya.jpg","medan.jpg","lombok.jpg"]
            cell.lblTitle.text = "Flight to \(dataStatic[indexPath.row])"
            
            cell.imgFlight.image = UIImage.init(named: "\(dataStaticImg[indexPath.row])")
            cell.imgFlight.clipsToBounds = true
            cell.imgFlight.contentMode = .scaleAspectFill
        }
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 4
        
        return cell
    }
}
