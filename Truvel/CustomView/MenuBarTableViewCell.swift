//
//  MenuBarTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class MenuBarTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MenuBarTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuBarTableViewCell.identifier, for: indexPath) as! MenuBarTableViewCell
        
        return cell
    }
}
