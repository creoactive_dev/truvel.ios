//
//  TopFlightTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

protocol ccollectionDelegate: class {
    func onDismiss(index:IndexPath)
}

class TopFlightTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            let xib = TopFlightHomeCollectionViewCell.nib
            collectionView.register(xib, forCellWithReuseIdentifier: TopFlightHomeCollectionViewCell.identifier)
                
            collectionView.delegate = self
            collectionView.dataSource = self
        }
    }
   internal var context: UIViewController?
    @IBOutlet weak var lblTitleRow: UILabel!
    var tabFrom : Int = 0{
        didSet{
            collectionView.reloadData()
        }
    }
    
    var delegate : ccollectionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension TopFlightTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: TopFlightTableViewCell.identifier, for: indexPath) as! TopFlightTableViewCell
        cell.context = context
        
        return cell
    }
}

extension TopFlightTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let del = delegate {
            del.onDismiss(index: indexPath)
        }
    }
    
}

extension TopFlightTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = tabFrom
        
        if let ctx = self.context {
            return TopFlightHomeCollectionViewCell.configure(context: ctx, collectionView: collectionView, indexPath: indexPath, object: data)
        } else {
            return UICollectionViewCell()
        }
        
        
    }
}

extension TopFlightTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2.5, height: collectionView.frame.size.height)
    }
}

