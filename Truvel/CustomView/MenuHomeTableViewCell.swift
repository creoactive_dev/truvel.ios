//
//  MenuHomeTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class MenuHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var imgHotel: UIImageView!
    @IBOutlet weak var lblHotel: UILabel!
    @IBOutlet weak var imgFlight: UIImageView!
    @IBOutlet weak var lblFlight: UILabel!
    @IBOutlet weak var lineHotel: UIView!
    @IBOutlet weak var lineFlight: UIView!
    @IBOutlet weak var btnFlight: UIButton!
    @IBOutlet weak var btnHotel: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MenuHomeTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuHomeTableViewCell.identifier, for: indexPath) as! MenuHomeTableViewCell
            return cell
    }
}

