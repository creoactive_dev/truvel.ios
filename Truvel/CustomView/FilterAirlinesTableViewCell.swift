//
//  FilterAirlinesTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class FilterAirlinesTableViewCell: UITableViewCell {

    @IBOutlet weak var btnSelected: Checkbox!
    @IBOutlet weak var imgFlight: UIImageView!
    @IBOutlet weak var lblAirlines: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnSelected.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension FilterAirlinesTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterAirlinesTableViewCell.identifier, for: indexPath) as! FilterAirlinesTableViewCell
        var airlines = ["Garuda Airlines", "Batik Air", "Trigana Air", "NAM Air", "Wings Air", "Lion Air", "Kalstar", "Malindo Air", "Citilink", "Air Asia", "Sriwijaya"]
        cell.imgFlight.image = UIImage(named: "ico-flight\(indexPath.row)")
        cell.lblAirlines.text = airlines[indexPath.row]
        return cell
    }
}

extension FilterAirlinesTableViewCell: CheckboxDelegate {
    func didSelect(_ checkbox: Checkbox) {
        switch checkbox {
        case btnSelected:
            btnSelected.setImage(#imageLiteral(resourceName: "ico-calendar-active"), for: .selected)
            print("checkbox one selected")
            break
        default:
            break
        }
    }
    
    func didDeselect(_ checkbox: Checkbox) {
        switch checkbox {
        case btnSelected:
            print("checkbox one deselected")
            break
        default:
            break
        }
    }
}
