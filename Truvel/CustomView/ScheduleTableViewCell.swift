//
//  ScheduleTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/14/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class ScheduleTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCodeDeparture: UILabel!
    @IBOutlet weak var lblCodeTransit1: UILabel!
    @IBOutlet weak var lblCodeTransit2: UILabel!
    @IBOutlet weak var lblCodeArrival: UILabel!
    @IBOutlet weak var lblAirlines: UILabel!
    @IBOutlet weak var lblCodeFlight: UILabel!
    @IBOutlet weak var imgAirlines: UIImageView!
    @IBOutlet weak var lblDepartureTime: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblArrivalTime: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgDirect: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ScheduleTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleTableViewCell.identifier, for: indexPath) as! ScheduleTableViewCell
        return cell
    }
}
