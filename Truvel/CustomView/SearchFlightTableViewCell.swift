//
//  SearchFlightTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class SearchFlightTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblAirport: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SearchFlightTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchFlightTableViewCell.identifier, for: indexPath) as! SearchFlightTableViewCell
        guard let data = object as? ListAirport else { return cell }
        
//        let dataCity = ["JKTA - Jakarta All","DPS - Denpasar","SUB - Surabaya", "JOG - Yogyakarta", "KNO - Medan", "UPG - Makassar", "BDO - Bandung"]
//        let dataAirport = ["All Airport in Jakarta", "Ngurah Rai, Bali", "Juanda, Surabaya", "Adi Sutjipto, Yogyakarta", "Kualanamu, Medan", "Hassanudin, Makassar", "Husein Sastranegara, Bandung"]
     
        cell.lblCity.text = "\(data.code) - \(data.city)"
        cell.lblAirport.text = data.name
        
        return cell
    }
}
