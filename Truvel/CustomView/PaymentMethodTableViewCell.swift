//
//  PaymentMethodTableViewCell.swift
//  Truvel
//
//  Created by Calista on 11/30/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {
    @IBOutlet weak var lblMethod: UILabel!
    @IBOutlet weak var imgMethod1: UIImageView!
    @IBOutlet weak var imgMethod2: UIImageView!
    @IBOutlet weak var viewBottom: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PaymentMethodTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PaymentMethodTableViewCell.identifier, for: indexPath) as! PaymentMethodTableViewCell
        
        return cell
    }
}
