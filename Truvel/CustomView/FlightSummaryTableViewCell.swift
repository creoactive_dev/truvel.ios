//
//  FlightSummaryTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class FlightSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDepartTime: UILabel!
    @IBOutlet weak var lblDepartDate: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblArrivalTime: UILabel!
    @IBOutlet weak var lblArrivalDate: UILabel!
    @IBOutlet weak var lblDepartAirport: UILabel!
    @IBOutlet weak var lblDepartCity: UILabel!
    @IBOutlet weak var lblTransitAirport: UILabel!
    @IBOutlet weak var lblTransitCity: UILabel!
    @IBOutlet weak var lblTransit2Airport: UILabel!
    @IBOutlet weak var lblTransit2City: UILabel!
    @IBOutlet weak var lblTransit3Airport: UILabel!
    @IBOutlet weak var lblTransit3City: UILabel!
    @IBOutlet weak var lblArrivalAirport: UILabel!
    @IBOutlet weak var lblArrivalCity: UILabel!
    @IBOutlet weak var lblBaggage: UILabel!
    @IBOutlet weak var lblMeal: UILabel!
    @IBOutlet weak var viewTransit: UIView!
    @IBOutlet weak var viewTransit2: UIView!
    @IBOutlet weak var viewTransit3: UIView!
    @IBOutlet weak var imgTransit: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension FlightSummaryTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FlightSummaryTableViewCell.identifier, for: indexPath) as! FlightSummaryTableViewCell
        guard let datas = object as? FlightJourneys else { return cell}
        let data = datas.flightSegments[0]
        let deFormatter = DateFormatter()
        deFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let startTime = deFormatter.date(from: data.std)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: startTime!)
        
        let dtFormatter1 = DateFormatter()
        dtFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let startTime1 = dtFormatter1.date(from: data.sta)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "HH:mm"
        let timeString1 = formatter1.string(from: startTime1!)
        
        cell.lblDepartTime.text = timeString
        cell.lblArrivalTime.text = timeString1
        cell.lblDuration.text = "\(data.duration.hours) h \(data.duration.minutes) m"
        
        cell.lblDepartAirport.text = "\(data.departureAirport.city) (\(data.departureAirport.cityCode))"
        cell.lblArrivalAirport.text = "\(data.arrivalAirport.city) (\(data.arrivalAirport.cityCode))"
        cell.lblDepartCity.text = data.departureAirport.name
        cell.lblArrivalCity.text = data.arrivalAirport.name
        
        if datas.stopCount == 0 {
            cell.viewTransit.isHidden = true
            cell.viewTransit2.isHidden = true
            cell.viewTransit3.isHidden = true
            cell.imgTransit.image = #imageLiteral(resourceName: "ico-flight-line-vert-direct")
        }else if datas.stopCount == 1{
            cell.viewTransit.isHidden = true
            cell.viewTransit2.isHidden = true
            cell.imgTransit.image = #imageLiteral(resourceName: "ico-flight-line-vert1-transit")
            
            let data1 = datas.flightSegments[0]
            let data2 = datas.flightSegments[1]
            cell.lblTransit3Airport.text = "\(data1.arrivalAirport.city) (\(data1.arrivalAirport.cityCode))"
            cell.lblTransit3City.text = data1.arrivalAirport.name
            
            cell.lblArrivalAirport.text = "\(data2.arrivalAirport.city) (\(data2.arrivalAirport.cityCode))"
            cell.lblArrivalCity.text = data2.arrivalAirport.name
        }else {
            cell.viewTransit3.isHidden = true
            cell.imgTransit.image = #imageLiteral(resourceName: "ico-flight-line-vert2-transit")
            
            let data1 = datas.flightSegments[0]
            let data2 = datas.flightSegments[1]
            let data3 = datas.flightSegments[2]
            cell.lblTransitAirport.text = "\(data1.arrivalAirport.city) (\(data1.arrivalAirport.cityCode))"
            cell.lblTransitCity.text = data1.arrivalAirport.name
            
            cell.lblTransit2Airport.text = "\(data2.arrivalAirport.city) (\(data2.arrivalAirport.cityCode))"
            cell.lblTransit2City.text = data2.arrivalAirport.name
            
            cell.lblArrivalAirport.text = "\(data3.arrivalAirport.city) (\(data3.arrivalAirport.cityCode))"
            cell.lblArrivalCity.text = data3.arrivalAirport.name
        }
        
        cell.lblBaggage.text = "Baggage - \(data.includedBaggage) kg"
        if data.includedMeal == 0 {
            cell.lblMeal.text = "No Meal"
        } else {
            cell.lblMeal.text = "Include Meal"
        }
        
        return cell
    }
}

