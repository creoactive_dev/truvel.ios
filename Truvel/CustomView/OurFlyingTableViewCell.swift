//
//  OurFlyingTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class OurFlyingTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet {
            let xib = OurFlightCollectionViewCell.nib
            collectionView.register(xib, forCellWithReuseIdentifier: OurFlightCollectionViewCell.identifier)
            
            collectionView.delegate = self
            collectionView.dataSource = self
            
            collectionView.isScrollEnabled = false
        }

    }
    
    @IBOutlet weak var lblTitleRow: UILabel!
    internal var context: UIViewController?
    var tabFrom: Int = 0 {
        didSet{
            collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension OurFlyingTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: OurFlyingTableViewCell.identifier, for: indexPath) as! OurFlyingTableViewCell
        
        return cell
    }
}

extension OurFlyingTableViewCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

}

extension OurFlyingTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = tabFrom
        
        if let ctx = self.context {
            return OurFlightCollectionViewCell.configure(context: ctx, collectionView: collectionView, indexPath: indexPath, object: data)
        } else {
            return UICollectionViewCell()
        }
        
        
    }
}

extension OurFlyingTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if tabFrom == 0 {
            return CGSize(width: collectionView.frame.size.width/4, height: 30)
            
        }else if tabFrom == 1{
            return CGSize(width: collectionView.frame.size.width/6, height: 50)
        }
        return CGSize(width: 0, height: 0)
    }
}
