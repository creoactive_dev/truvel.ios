//
//  BirthdateTableViewCell.swift
//  Truvel
//
//  Created by Calista on 11/16/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class BirthdateTableViewCell: UITableViewCell {
    @IBOutlet weak var txtBirthdate: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension BirthdateTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BirthdateTableViewCell.identifier, for: indexPath) as! BirthdateTableViewCell
                
        return cell
    }
}
