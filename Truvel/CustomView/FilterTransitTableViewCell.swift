//
//  FilterTransitTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class FilterTransitTableViewCell: UITableViewCell {
    @IBOutlet weak var btnDirect: UIButton!
    @IBOutlet weak var btnOneTransit: UIButton!
    @IBOutlet weak var btnMoreTransits: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension FilterTransitTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterTransitTableViewCell.identifier, for: indexPath) as! FilterTransitTableViewCell
        cell.btnDirect.layer.cornerRadius = 8
        cell.btnOneTransit.layer.cornerRadius = 8
        cell.btnMoreTransits.layer.cornerRadius = 8
        return cell
    }
}
