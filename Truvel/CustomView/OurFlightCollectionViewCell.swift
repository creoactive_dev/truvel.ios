//
//  OurFlightCollectionViewCell.swift
//  Truvel
//
//  Created by Calista on 10/16/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class OurFlightCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgFlight: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
extension OurFlightCollectionViewCell: CollectionViewCellProtocol{
    static func configure<T>(context: UIViewController, collectionView: UICollectionView, indexPath: IndexPath, object: T) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OurFlightCollectionViewCell.identifier, for: indexPath) as! OurFlightCollectionViewCell
          guard let data = object as? Int else { return cell }
        if data == 0 {
            cell.imgFlight.image = UIImage(named: "ico-hotel\(indexPath.row)")
        }else if data == 1{
            cell.imgFlight.image = UIImage(named: "ico-flight\(indexPath.row)")
        }
       
      
        return cell
    }
}

