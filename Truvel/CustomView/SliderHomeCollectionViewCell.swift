//
//  SliderHomeCollectionViewCell.swift
//  Truvel
//
//  Created by Calista on 10/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class SliderHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgSlider: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension SliderHomeCollectionViewCell: CollectionViewCellProtocol{
    static func configure<T>(context: UIViewController, collectionView: UICollectionView, indexPath: IndexPath, object: T) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SliderHomeCollectionViewCell.identifier, for: indexPath) as! SliderHomeCollectionViewCell
        
        let dataStaticImg = ["1.jpg","2.jpg","3.jpg","4.jpg"]
        cell.imgSlider.image = UIImage.init(named: "\(dataStaticImg[indexPath.row])")
        cell.imgSlider.clipsToBounds = true
        cell.imgSlider.contentMode = .scaleAspectFill
        
        return cell
    }
}
