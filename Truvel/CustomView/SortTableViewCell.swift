//
//  SortTableViewCell.swift
//  Truvel
//
//  Created by Calista on 11/5/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class SortTableViewCell: UITableViewCell {
    @IBOutlet weak var imgSort: UIImageView!
    @IBOutlet weak var lblSort: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}

extension SortTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SortTableViewCell.identifier, for: indexPath) as! SortTableViewCell
        var sorts = ["Lowest price", "Highest price", "Earliest departure", "Latest departure", "Shortest duration"]
        cell.imgSort.image = UIImage(named: "ico-sort\(indexPath.row)")
        cell.lblSort.text = sorts[indexPath.row]
        return cell
    }
}
