//
//  PriceDetailTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class PriceDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var lblAdultFare: UILabel!
    @IBOutlet weak var lblChildFare: UILabel!
    @IBOutlet weak var lblInfantFare: UILabel!
    @IBOutlet weak var lblPriceAdult: UILabel!
    @IBOutlet weak var lblPriceChild: UILabel!
    @IBOutlet weak var lblPriceInfant: UILabel!
    @IBOutlet weak var lblVAT: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblTotalPay: UILabel!
    @IBOutlet weak var topViewVAT: NSLayoutConstraint!
    @IBOutlet weak var topViewChild: NSLayoutConstraint!
    @IBOutlet weak var topViewInfant: NSLayoutConstraint!
    @IBOutlet weak var viewChild: UIView!
    @IBOutlet weak var viewInfant: UIView!
    @IBOutlet weak var heightViewChild: NSLayoutConstraint!
    
    @IBOutlet weak var heightViewInfant: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PriceDetailTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PriceDetailTableViewCell.identifier, for: indexPath) as! PriceDetailTableViewCell
       // guard let data = object as? FlightSegments else { return cell}
        
        return cell
    }
}
