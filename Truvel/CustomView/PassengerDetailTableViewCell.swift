//
//  PassengerDetailTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/20/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class PassengerDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnFindDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension PassengerDetailTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PassengerDetailTableViewCell.identifier, for: indexPath) as! PassengerDetailTableViewCell
//        
//        if indexPath.row == 0 {
//            cell.lblName.text = "Mr Ujang Sulistyo"
//            cell.btnFindDetail.text = "EDIT"
//            cell.lblName.font = UIFont(name: "Nunito-Bold", size: 14)!
//        }else {
//            cell.lblName.text = "Passenger (Adult)"
//            cell.btnFindDetail.text = "FIND IN DETAILS"
//            cell.lblName.font = UIFont(name: "Nunito-Regular", size: 14)!
//        }
        return cell
    }
}
