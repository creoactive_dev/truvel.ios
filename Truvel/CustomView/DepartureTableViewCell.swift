//
//  DepartureTableViewCell.swift
//  Truvel
//
//  Created by Calista on 11/30/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class DepartureTableViewCell: UITableViewCell {
    @IBOutlet weak var imgFlight: UIImageView!
    
    @IBOutlet weak var lblAirlines: UILabel!
    @IBOutlet weak var lblCodeFlight: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnPriceDetail: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension DepartureTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DepartureTableViewCell.identifier, for: indexPath) as! DepartureTableViewCell
        
        return cell
    }
}
