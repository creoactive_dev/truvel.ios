//
//  ContactDetailTableViewCell.swift
//  Truvel
//
//  Created by Calista on 10/20/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

protocol contactDelegate: class {
    func fullnameDidChange(name: String, title: String)
    func phoneNoDidChange(no: String)
    func emailDidChange(email: String)
}


class ContactDetailTableViewCell: UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var btnTitle: UIButton!
    @IBOutlet weak var btnCountryCoude: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnPassenger: UIButton!
    
    var delegate: contactDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.txtEmail.delegate = self
        self.txtFullname.delegate = self
    }

    @IBAction func fullnameDidChange(_ sender: Any) {
        print("\(txtFullname.text!)")
        if let name = txtFullname.text {
            delegate?.fullnameDidChange(name: name, title: lblTitle.text!)
        }
    }
    
    @IBAction func phoneNoDidChange(_ sender: Any) {
        print("\(txtPhoneNumber.text!)")
        if let phone = txtPhoneNumber.text{
            delegate?.phoneNoDidChange(no: phone)
        }
    }
    @IBAction func emailDidChange(_ sender: Any) {
        if let email = txtEmail.text{
            delegate?.emailDidChange(email: email)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }

}

extension ContactDetailTableViewCell: TableViewCellProtocol {
    static func configure<T>(context: UIViewController, tableView: UITableView, indexPath: IndexPath, object: T) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContactDetailTableViewCell.identifier, for: indexPath) as! ContactDetailTableViewCell
        
        return cell
    }
}
