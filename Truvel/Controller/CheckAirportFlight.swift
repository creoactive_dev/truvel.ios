//
//  CheckAirportFlight.swift
//  Truvel
//
//  Created by Calista on 10/31/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire


class CheckAirportFlight: BaseController{
    fileprivate let listAirportAPI = REST.ServiceAPI.Airport.listAirport.ENV
    fileprivate let airAvailableAPI = REST.ServiceAPI.Airport.airAvailable.ENV
    fileprivate let filterAPI = REST.ServiceAPI.Airport.filter.ENV
    
    func getListAirport(onSuccess: @escaping CollectionResultListener<ListAirport>,
                        onFailed: @escaping MessageListener,
                        onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: listAirportAPI, param: nil, method: .get) {
            (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["error"][0]["code"].intValue == 000{
                    var airports = [ListAirport]()
                    for value in data["airports"].arrayValue {
                        let airport = ListAirport(json: value)
                        airports.append(airport)
                    }
                    
                    onSuccess(200, "Success fetching data", airports)
                    onComplete("Fetching data completed")
                }
            }else{
                if statusCode >= 400 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func getAirAvailability(depart: String, arrival: String, departureDate: String, returnDate: String, flightCode: String, noOfAdt: String, noOfChd: String, noOfInf: String,
                            onSuccess: @escaping CollectionResultListener<AirAvailable>,
                            onFailed: @escaping MessageListener,
                            onComplete: @escaping MessageListener) {
        var param : Parameters
        if returnDate == ""{
            param = [
                "departureAirport": ["code":"\(depart)"],
                "arrivalAirport" : ["code": "\(arrival)"],
                "departureDate" : departureDate,
                "returnDate": returnDate,
                "currency":"IDR",
                "airlines":[
                    ["code": flightCode]
                ],
                "noOfAdt": noOfAdt,
                "noOfChd": noOfChd,
                "noOfInf": noOfInf
            ]
            /*
             ["code": "GA"],
             ["code": "IL"],
             ["code": "QZ"],
             ["code": "KD"],
             ["code": "QG"],
             ["code": "SJ"]
             
             */
        }else{
           param = [
                "departureAirport": ["code":"\(depart)"],
                "arrivalAirport" : ["code": "\(arrival)"],
                "departureDate" : departureDate,
                "returnDate": returnDate,
                "currency":"IDR",
                "airlines":[
                    ["code": "JT"],
                    ["code": "GA"],
                    ["code": "QG"],
                    ["code": "SJ"]
                    ],
                "noOfAdt": noOfAdt,
                "noOfChd": noOfChd,
                "noOfInf": noOfInf
            ]
        }
        let prms = JSON(param).rawString()
        print(prms ?? "kosong cuy")
        //"2018-05-01T00:00:00"
        httpHelper.requestJSON(url: airAvailableAPI, param: param, method: .post) { (success, statusCode, json) in
            
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                print(data)
                                
                if data["error"][0]["code"].intValue == 000{
                    var airports = [AirAvailable]()
                    for value in data["flightSchedules"].arrayValue {
                        let airport = AirAvailable(json: value)
                        airports.append(airport)
                    }
                                       
                    onSuccess(200, "Success fetching data", airports)
                    onComplete("Fetching data completed")
                }
            }else{
                if statusCode >= 400 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    func postFilter(sortPrice: String, priceMin: String, priceMax: String, stop: String,
                    onSuccess: @escaping CollectionResultListener<AirAvailable>,
                    onFailed: @escaping MessageListener,
                    onComplete: @escaping MessageListener) {
        let param : Parameters = ["sort_price": sortPrice,
                                  "price_range_min" : priceMin,
                                  "price_range_max" : priceMax,
                                  "stops_stop" : stop,
                                  "airlines" : ["JT","GA"],
                                  "data" : ["errors": [["code": "000", "message": "Success.","type": "SUCCESS"]],
                                            "responseTime": "593",
                                            "timestamp": "/Date(1496721274070+0700)/",
                                            "flightSchedules": [[
                                                "arrivalAirport": [
                                                    "city": "DPS",
                                                    "cityCode": "",
                                                    "code": "DPS",
                                                    "countryCode": "ID",
                                                    "latitude": "-8.748169",
                                                    "longitude": "115.167172",
                                                    "name": "Bali Ngurah Rai, Denpasar",
                                                    "timezone": ""
                                                    ],
                                                "departureAirport": [
                                                    "city": "LOP",
                                                    "cityCode": "",
                                                    "code": "LOP",
                                                    "countryCode": "ID",
                                                    "latitude": "-8.7573222",
                                                    "longitude": "116.276675",
                                                    "name": "Lombok Intl, Lombok",
                                                    "timezone": ""
                                                    ],
                                                "flightJourneys": [[
                                                    "arrivalAirport": [
                                                        "city": "DPS",
                                                        "cityCode": "",
                                                        "code": "DPS",
                                                        "countryCode": "ID",
                                                        "latitude": "-8.748169",
                                                        "longitude": "115.167172",
                                                        "name": "Bali Ngurah Rai, Denpasar",
                                                        "timezone": ""
                                                    ],
                                                    "departureAirport": [
                                                        "city": "LOP",
                                                        "cityCode": "",
                                                        "code": "LOP",
                                                        "countryCode": "ID",
                                                        "latitude": "-8.7573222",
                                                        "longitude": "116.276675",
                                                        "name": "Lombok Intl, Lombok",
                                                        "timezone": ""
                                                    ],
                                                    "flightSegments": [[
                                                        "GDSCode": "",
                                                        "aircraftCode": "",
                                                        "airline": [
                                                            "code": "IW",
                                                            "name": "Wings Air"
                                                        ],
                                                        "arrivalAirport": [
                                                            "city": "DPS",
                                                            "cityCode": "",
                                                            "code": "DPS",
                                                            "countryCode": "ID",
                                                            "latitude": "-8.748169",
                                                            "longitude": "115.167172",
                                                            "name": "Bali Ngurah Rai, Denpasar",
                                                            "timezone": ""
                                                        ],
                                                        "departureAirport": [
                                                            "city": "LOP",
                                                            "cityCode": "",
                                                            "code": "LOP",
                                                            "countryCode": "ID",
                                                            "latitude": "-8.7573222",
                                                            "longitude": "116.276675",
                                                            "name": "Lombok Intl, Lombok",
                                                            "timezone": ""
                                                        ],
                                                        "duration": [
                                                            "hours": "0",
                                                            "minutes": "30",
                                                            "seconds": "0"
                                                            ],
                                                        "fares": [[
                                                            "OtherFare": "",
                                                            "adminFee": "0",
                                                            "airportTax": "45000",
                                                            "availableCount": "7",
                                                            "baggageAllowance": "0",
                                                            "basicFare": "209000",
                                                            "basicVat": "0",
                                                            "childFare": "209000",
                                                            "childVat": "0",
                                                            "classOfService": "M",
                                                            "currency": "IDR",
                                                            "discount": "0",
                                                            "fareSellKey": "M0_C0_F0_S14",
                                                            "fareType": "NR",
                                                            "fuelSurcharge": "0",
                                                            "infantFare": "0",
                                                            "infantVat": "0",
                                                            "insurance": "0",
                                                            "iwjr": "5000",
                                                            "otherFee": "0",
                                                            "serviceFee": "0"
                                                            ]],
                                                        "flightDesignator": [
                                                            "carrierCode": "IW",
                                                            "flightNumber": "1857"
                                                        ],
                                                        "id": "",
                                                        "includedBaggage": "15",
                                                        "includedMeal": "0",
                                                        "segmentSellKey": "IW~1857~ ~~LOP~06/13/2017 07:50~DPS~06/13/2017 08:20~",
                                                        "selectedFare": "",
                                                        "sta": "2017-06-13T08:20:00",
                                                        "std": "2017-06-13T07:50:00",
                                                        "stopCount": "0",
                                                        "stopLeg": ""
                                                        ]],
                                                    "journeySellKey": "IW~1857~ ~~LOP~06/13/2017 07:50~DPS~06/13/2017 08:20~",
                                                    "stopCount": "0"
                                                    ],
                                                [
                                                    "arrivalAirport": [
                                                        "city": "DPS",
                                                        "cityCode": "",
                                                        "code": "DPS",
                                                        "countryCode": "ID",
                                                        "latitude": "-8.748169",
                                                        "longitude": "115.167172",
                                                        "name": "Bali Ngurah Rai, Denpasar",
                                                        "timezone": ""
                                                    ],
                                                    "departureAirport": [
                                                        "city": "LOP",
                                                        "cityCode": "",
                                                        "code": "LOP",
                                                        "countryCode": "ID",
                                                        "latitude": "-8.7573222",
                                                        "longitude": "116.276675",
                                                        "name": "Lombok Intl, Lombok",
                                                        "timezone": ""
                                                    ],
                                                    "flightSegments": [[
                                                        "GDSCode": "",
                                                        "aircraftCode": "",
                                                        "airline": [
                                                            "code": "IW",
                                                            "name": "Wings Air"
                                                        ],
                                                        "arrivalAirport": [
                                                            "city": "DPS",
                                                            "cityCode": "",
                                                            "code": "DPS",
                                                            "countryCode": "ID",
                                                            "latitude": "-8.748169",
                                                            "longitude": "115.167172",
                                                            "name": "Bali Ngurah Rai, Denpasar",
                                                            "timezone": ""
                                                        ],
                                                        "departureAirport": [
                                                            "city": "LOP",
                                                            "cityCode": "",
                                                            "code": "LOP",
                                                            "countryCode": "ID",
                                                            "latitude": "-8.7573222",
                                                            "longitude": "116.276675",
                                                            "name": "Lombok Intl, Lombok",
                                                            "timezone": ""
                                                        ],
                                                        "duration": [
                                                            "hours": "0",
                                                            "minutes": "30",
                                                            "seconds": "0"
                                                        ],
                                                        "fares": [[
                                                            "OtherFare": "",
                                                            "adminFee": "0",
                                                            "airportTax": "45000",
                                                            "availableCount": "3",
                                                            "baggageAllowance": "0",
                                                            "basicFare": "242000",
                                                            "basicVat": "0",
                                                            "childFare": "242000",
                                                            "childVat": "0",
                                                            "classOfService": "K",
                                                            "currency": "IDR",
                                                            "discount": "0",
                                                            "fareSellKey": "M0_C2_F0_S12",
                                                            "fareType": "NR",
                                                            "fuelSurcharge": "0",
                                                            "infantFare": "0",
                                                            "infantVat": "0",
                                                            "insurance": "0",
                                                            "iwjr": "5000",
                                                            "otherFee": "0",
                                                            "serviceFee": "0"
                                                            ]],
                                                        "flightDesignator": [
                                                            "carrierCode": "IW",
                                                            "flightNumber": "1851"
                                                            ],
                                                        "id": "",
                                                        "includedBaggage": "15",
                                                        "includedMeal": "0",
                                                        "segmentSellKey": "IW~1851~ ~~LOP~06/13/2017 11:45~DPS~06/13/2017 12:15~",
                                                        "selectedFare": "",
                                                        "sta": "2017-06-13T12:15:00",
                                                        "std": "2017-06-13T11:45:00",
                                                        "stopCount": "0",
                                                        "stopLeg": ""
                                                        ]],
                                                    "journeySellKey": "IW~1851~ ~~LOP~06/13/2017 11:45~DPS~06/13/2017 12:15~",
                                                    "stopCount": "0"
                                                    ]]
                                                ]]
                                        ]
                            ]
        
        httpHelper.requestJSON(url: filterAPI, param: param, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                print("result filter \(data)")
                if data["error"][0]["code"].intValue == 000 {
//                    var airports = [AirAvailable]()
//                    for value in data["flightSchedules"].arrayValue {
//                        let airport = AirAvailable(json: value)
//                        airports.append(airport)
//                    }
//                    
//                    onSuccess(200, "Success fetching data", airports)
//                    onComplete("Fetching data completed")
                }
            }else{
                if statusCode >= 400 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    
    }
    /*
    
     "sort_price":"descending",
     "price_range_min":0,
     "price_range_max":1000000,
     "stops_stop":"all",
     "airlines":["IW","JT"],
     "data":{}
     */
}
