//
//  ListAirportController.swift
//  Truvel
//
//  Created by Calista on 10/31/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import SwiftyJSON


class ListAirportController: BaseController{
    fileprivate let listAirportAPI = REST.ServiceAPI.Airport.listAirport.ENV
    
    func getListAirport(onSuccess: @escaping CollectionResultListener<ListAirport>,
                         onFailed: @escaping MessageListener,
                         onComplete: @escaping MessageListener) {
        
        httpHelper.request(url: listAirportAPI, param: nil, method: .get) {
            (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                if data["error"][0]["code"].intValue == 000 {
                    var airports = [ListAirport]()
                    for value in data["airports"].arrayValue {
                        let airport = ListAirport(json: value)
                        airports.append(airport)
                    }
                    
                    onSuccess(200, "Success fetching data", airports)
                    onComplete("Fetching data completed")

                }
           
            } else{
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
}
