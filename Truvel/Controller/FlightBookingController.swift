//
//  FlightBookingController.swift
//  Truvel
//
//  Created by Calista on 11/19/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class FlightBookingController: BaseController{
    fileprivate let bookFlightAPI = REST.ServiceAPI.Booking.bookFlight.ENV
    fileprivate let invoiceNoAPI = REST.ServiceAPI.Invoice.doInvoiceData.ENV
    fileprivate let paymentNotifCCAPI = REST.ServiceAPI.Payment.cc
    fileprivate let paymentNotifMCAPI = REST.ServiceAPI.Payment.mc
    fileprivate let paymentNotifVAAPI = REST.ServiceAPI.Payment.va

    func getBookFlight(fname: String, mName: String, lName: String, title: String, mphone : String, email: String, city: String, passenger: [Passengers], paxFare:[Fares], infant: String, flightJourney : FlightJourneys, onSuccess: @escaping SingleResultListener<String>,
                       onFailed: @escaping MessageListener,
                       onComplete: @escaping MessageListener) {
        let fare = flightJourney.flightSegments[0].fares[0]

        // Ikutin kata digit
        var passengerDictionary = [[String : Any]]()
        for val in passenger {
            let teet = [
                "passengerNumber" : val.passNum,
                "idNo": "",
                "paxType": val.paxType,
                "firstName": val.fName,
                "middleName": val.mName,
                "lastName": val.lName,
                "title": val.title,
                "dob": "1990-01-01T06:00:00",
                "gender": val.gender,
                "nationality": val.nationality,
                "ticketNumber": "",
                "infant": NSNull(),
                "paxFare": [
                    "fareSellKey": paxFare[0].fareSellKey,
                    "fareType": paxFare[0].fareType,
                    "classOfService": paxFare[0].classOfService,
                    "currency": paxFare[0].currency,
                    "basicFare": paxFare[0].basicFare,
                    "basicVat": paxFare[0].basicVat,
                    "iwjr": paxFare[0].iwjr,
                    "airportTax": paxFare[0].airportTax,
                    "fuelSurcharge": paxFare[0].fuelSurcharge,
                    "adminFee": paxFare[0].adminFee,
                    "baggageAllowance": paxFare[0].baggageAllowance,
                    "insurance": paxFare[0].insurance,
                    "otherFee": paxFare[0].otherFee
                ]
                
            ] as [String : Any]
            
            passengerDictionary.append(teet)
        }
        // Dah sampe sini
        
        let param = ["contacts" : [[ "firstName": fname,
                                                  "middleName": mName,
                                                  "lastName": lName,
                                                  "title": title,
                                                  "homePhone": "",
                                                  "mobilePhone": mphone,
                                                  "workPhone": "",
                                                  "email": email,
                                                  "city": "",
                                                  "postalCode": "",
                                                  "addressLine1": "",
                                                  "addressLine2": "",
                                                  "addressLine3": ""

                                ]],
                                  "passengers": passengerDictionary,
                                  "flightJourneys" : [
                                    [
                                        "arrivalAirport":[
                                            "city":flightJourney.arrivalAirport.city,
                                            "cityCode":flightJourney.arrivalAirport.cityCode,
                                            "code":flightJourney.arrivalAirport.code,
                                            "countryCode":"ID",
                                            "latitude":flightJourney.arrivalAirport.latitude,
                                            "longitude":flightJourney.arrivalAirport.logitude,
                                            "name":flightJourney.arrivalAirport.name,
                                            "timezone":""
                                        ],
                                        "cabin":flightJourney.cabin,
                                        "departureAirport":[
                                            "city":flightJourney.departureAirport.city,
                                            "cityCode":flightJourney.departureAirport.cityCode,
                                            "code":flightJourney.departureAirport.code,
                                            "countryCode":"ID",
                                            "latitude":flightJourney.departureAirport.latitude,
                                            "longitude":flightJourney.departureAirport.logitude,
                                            "name":flightJourney.departureAirport.name,
                                            "timezone":""
                                        ],
                                        "flightSegments":[
                                        [
                                            "GDSCode":flightJourney.flightSegments[0].gdsCode,
                                            "aircraftCode":flightJourney.flightSegments[0].aircraftCode,
                                            "airline":[
                                                "code":flightJourney.flightSegments[0].airline.code,
                                                "name":flightJourney.flightSegments[0].airline.name
                                                ],
                                            "arrivalAirport":[
                                                "city":flightJourney.flightSegments[0].arrivalAirport.city,
                                                "cityCode":flightJourney.flightSegments[0].arrivalAirport.cityCode,
                                                "code":flightJourney.flightSegments[0].arrivalAirport.code,
                                                "countryCode":"ID",
                                                "latitude":flightJourney.flightSegments[0].arrivalAirport.latitude,
                                                "longitude":flightJourney.flightSegments[0].arrivalAirport.logitude,
                                                "name":flightJourney.flightSegments[0].arrivalAirport.name,
                                                "timezone":""
                                                ],
                                            "cabin":flightJourney.flightSegments[0].cabin,
                                            "departureAirport":[
                                                "city":flightJourney.flightSegments[0].departureAirport.city,
                                                "cityCode":flightJourney.flightSegments[0].departureAirport.cityCode,
                                                "code":flightJourney.flightSegments[0].departureAirport.code,
                                                "countryCode":"ID",
                                                "latitude":flightJourney.flightSegments[0].departureAirport.latitude,
                                                "longitude":flightJourney.flightSegments[0].departureAirport.logitude,
                                                "name":flightJourney.flightSegments[0].departureAirport.name,
                                                "timezone":""
                                                ],
                                            "duration":[
                                                "hours":flightJourney.flightSegments[0].duration.hours,
                                                "minutes":flightJourney.flightSegments[0].duration.minutes,
                                                "seconds":flightJourney.flightSegments[0].duration.seconds
                                                ],
                                        "fares":[[
                                            "OtherFare":"",
                                            "adminFee":fare.adminFee,
                                            "airportTax":fare.airportTax,
                                            "availableCount":fare.availableCount,
                                            "baggageAllowance":fare.baggageAllowance,
                                            "basicFare":fare.basicFare,
                                            "basicVat":fare.basicVat,
                                            "cabin":fare.cabin,
                                            "childFare":fare.childFare,
                                            "childVat":fare.childVat,
                                            "classOfService":fare.classOfService,
                                            "currency":"IDR",
                                            "discount":fare.discount,
                                            "fareSellKey":fare.fareSellKey,
                                            "fareType":fare.fareType,
                                            "fuelSurcharge":fare.fuelSurcharge,
                                            "infantFare":fare.infantFare,
                                            "infantVat":fare.infantVat,
                                            "insurance":fare.insurance,
                                            "iwjr":fare.iwjr,
                                            "otherFee":fare.otherFee,
                                            "serviceFee":fare.serviceFee,
                                            "uid":""
                                            ]],
                                        "flightDesignator":[
                                            "carrierCode":flightJourney.flightSegments[0].flightDesignator.carrierCode,
                                            "flightNumber":flightJourney.flightSegments[0].flightDesignator.flightNumber
                                            ],
                                        "id":"",
                                        "includedBaggage":flightJourney.flightSegments[0].includedBaggage,
                                        "includedMeal":flightJourney.flightSegments[0].includedMeal,
                                        "segmentSellKey":flightJourney.flightSegments[0].segmentSellKey,
                                        "selectedFare":[
                                            "OtherFare":"",
                                            "adminFee":fare.adminFee,
                                            "airportTax":fare.airportTax,
                                            "availableCount":fare.availableCount,
                                            "baggageAllowance":fare.baggageAllowance,
                                            "basicFare":fare.basicFare,
                                            "basicVat":fare.basicVat,
                                            "cabin":fare.cabin,
                                            "childFare":fare.childFare,
                                            "childVat":fare.childVat,
                                            "classOfService":fare.classOfService,
                                            "currency":"IDR",
                                            "discount":fare.discount,
                                            "fareSellKey":fare.fareSellKey,
                                            "fareType":fare.fareType,
                                            "fuelSurcharge":fare.fuelSurcharge,
                                            "infantFare":fare.infantFare,
                                            "infantVat":fare.infantVat,
                                            "insurance":fare.insurance,
                                            "iwjr":fare.iwjr,
                                            "otherFee":fare.otherFee,
                                            "serviceFee":fare.serviceFee,
                                            "uid":""
                                            ],
                                        "sta":flightJourney.flightSegments[0].sta,
                                        "std":flightJourney.flightSegments[0].std,
                                        "stopCount":flightJourney.flightSegments[0].stopCount,
                                        "stopLeg":"",
                                        "uid":""
                                        ]
                                        ],
                                    "journeySellKey":flightJourney.journeySellKey,
                                    "stopCount":flightJourney.stopCount,
                                    "uid":""
                                    ]
            ]
        ] as [String : Array<Dictionary<String, Any?>>]
        let prms = JSON(param).rawString()
        print(prms ?? "kosong cuy")
        httpHelper.requestJSON(url: bookFlightAPI, param: param, method: .post) {
            (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                let transactionID = data["transactionId"].stringValue
                
                if data["errors"][0]["code"].intValue == 000 {
                    onSuccess(200, "Success fetching data", transactionID)
                    onComplete("Fetching data completed")
                }else {
                    onSuccess(200, "Success fetching data",transactionID)  //"001110011177" "051110011175"
                }
            }else{
                if json?["errors"][0]["code"].intValue == 023 {
                    if let msg = json?["error"][0]["message"].stringValue{
                        onFailed(msg)
                    }
                    
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
    }
    
    // Agung Code
    func getInvNumber(amount: Int, currencyCode: String, transactID: String, onSuccess: @escaping CollectionResultListener<ListInvoice>,
                       onFailed: @escaping MessageListener,
                       onComplete: @escaping MessageListener) {
        
        let param = ["totalAmount":amount,"currencyCode":currencyCode,"transactionId":transactID] as [String : Any]
        
        httpHelper.requestJSON(url: invoiceNoAPI, param: param, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                var invoices = [ListInvoice]()
                for value in data.arrayValue {
                    let invoice = ListInvoice(json: value)
                    invoices.append(invoice)
                }
                
                onSuccess(200, "Success fetching data", invoices)
                onComplete("Fetching data completed")
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
        
    }
    
    func getPaymentNotif(amount: Int, currencyCode: String, transactID: String, param: [String: Any], onSuccess: @escaping SingleResultListener<String>,
                      onFailed: @escaping MessageListener,
                      onComplete: @escaping MessageListener) {
        
        /*let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let param = ["transactionId":transactID,"amount":amount,"currency":currencyCode,"paymentDate":format.string(from: date),"paymentChannel":"Cash"] as [String : Any]*/
        print(JSON(param).rawString() ?? "")
        httpHelper.requestJSON(url: paymentNotifCCAPI, param: param, method: .post) { (success, statusCode, json) in
            if success {
                guard let data = json else {
                    onFailed("Null response from server")
                    return
                }
                
                print(data)
                
                let message = data["errors"][0]["message"].stringValue
                
                if data["res_response_msg"].stringValue == "SUCCESS" {
                    onSuccess(200, "Success fetching data", "success")
                    onComplete("Fetching data completed")
                }else {
                    onSuccess(200, "Success fetching data",message)  //"001110011177" "051110011175"
                }
            } else {
                if json?["error"][0]["code"].intValue == 023 {
                    onFailed("Bad request")
                } else if statusCode >= 500 {
                    onFailed("Internal server error")
                } else {
                    onFailed("An error occured")
                }
            }
        }
        
    }
    
}
