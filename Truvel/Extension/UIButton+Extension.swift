//
//  UIButton+Extension.swift
//  Truvel
//
//  Created by Calista on 9/25/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

extension UIButton {
    var cornerRadius: CGFloat {
        return self.frame.height / 2
    }
}
