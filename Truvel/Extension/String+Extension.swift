//
//  String+Extension.swift
//  ProjectStructure
//
//  Created by Calista Bertha on 6/7/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import Foundation

extension String {
    var basePictureURL: String {
        return Config.basePictureAPI + self
    }
    
    var ENV: String {
        switch Config.baseAPI {
        case .development:
            return Config.developmentBaseAPI + self
        case .stagging:
            return Config.staggingBaseAPI + self
        case .production:
            return Config.productionBaseAPI + self
        }
    }
}
