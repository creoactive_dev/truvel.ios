//
//  ReviewBookingViewController.swift
//  Truvel
//
//  Created by Calista on 11/28/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class ReviewBookingViewController: UIViewController {
    @IBOutlet weak var lblBookingCode: UILabel!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = FlightSummaryTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: FlightSummaryTableViewCell.identifier)
            
            let xib1 = PriceDetailTableViewCell.nib
            table.register(xib1, forCellReuseIdentifier: PriceDetailTableViewCell.identifier)
            
            let xib2 = DepartureTableViewCell.nib
            table.register(xib2, forCellReuseIdentifier: DepartureTableViewCell.identifier)
            
            let xib3 = PassengerDetailTableViewCell.nib
            table.register(xib3, forCellReuseIdentifier: PassengerDetailTableViewCell.identifier)
            
            table.delegate = self
            table.dataSource = self
        }
    }
    @IBOutlet weak var viewNettPrice: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDepartAirport: UILabel!
    @IBOutlet weak var lblTransitAirport: UILabel!
    @IBOutlet weak var lblArrivalAirport: UILabel!
    @IBOutlet weak var imgDirect: UIImageView!
    @IBOutlet weak var btnContPayment: UIButton!
    @IBOutlet weak var btnPayment: UIButton!
    
    var transactionID : String?
    var total : Int?
    var data : FlightJourneys! = nil
    var noOfAdt = ""
    var noOfChd = ""
    var noOfInf = ""
    var amount: Int?
    var totPrice: String?
    var titleSection: [String] = [""]
    var titlePassenger: String?
    var namePassenger: String?
    var phoneNo : String?
    var email: String?
    
    var passengersColl: [Passengers] = [Passengers]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleSection = ["DEPARTURE FLIGHT", "PASSENGERS"]
        btnPayment.titleLabel?.lineBreakMode = .byWordWrapping
        btnPayment.titleLabel?.numberOfLines = 0
        viewNettPrice.layer.cornerRadius = 3
        btnContPayment.layer.cornerRadius = 5
        lblBookingCode.text = "Truvel Booking Code TVL\(transactionID!)"
        setViewBottom()
    }

    func setViewBottom(){
        if data.stopCount == 0 {
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-direct")
            lblTransitAirport.isHidden = true
            
        }else if data.stopCount == 1{
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-1-transit")
            lblTransitAirport.isHidden = false
            lblTransitAirport.text = "TRANSIT"
        }else {
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line2-transit")
        }
        
        lblDepartAirport.text = data.departureAirport.cityCode
        lblArrivalAirport.text = data.arrivalAirport.cityCode
        self.lblPrice.text = totPrice

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Action
    
    @IBAction func back(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func review(_ sender: Any) {
    }
    
    @IBAction func continuePayment(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.warning) as! WarningViewController

        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
        
        processBooking(controller: vc)
    }
    
    func processBooking(controller: WarningViewController) {
        controller.viewWarning.isHidden = true
        controller.viewWaiting.isHidden = false
        controller.indicator.type = DGActivityIndicatorAnimationType.ballClipRotate
        controller.indicator.tintColor = UIColor.white //UIColor(hexString: "00A49E")
        
        controller.indicator.startAnimating()
        
        FlightBookingController().getInvNumber(amount: self.amount!, currencyCode: "IDR", transactID: self.transactionID!, onSuccess: { (code, message, results) in
            
            controller.indicator.stopAnimating()
            controller.willMove(toParentViewController: nil)
            controller.view.removeFromSuperview()
            
            guard let res = results else { return }
            print(res[0].invoiceNo)
            let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.payment) as! PaymentViewController
            vc.totPrice = self.totPrice
            vc.amount = self.amount
            vc.data = self.data
            vc.phoneNo = self.phoneNo
            vc.transactionID = self.transactionID
            vc.invoiceNo = res[0].invoiceNo
            vc.titlePassenger = self.titlePassenger
            vc.namePassenger = self.namePassenger
            vc.phoneNo = self.phoneNo
            vc.email = self.email
            
            self.navigationController?.pushViewController(vc, animated: true)
        }, onFailed: { (message) in
            print(message)
        }, onComplete: { (message) in
            print(message)
        })
    }
    
}

extension ReviewBookingViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 3
        }else{
            return total!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let data1 = self.data.flightSegments[0]
                let cell = tableView.dequeueReusableCell(withIdentifier: DepartureTableViewCell.identifier, for: indexPath) as! DepartureTableViewCell
               
                cell.lblAirlines.text = data1.airline.name
                cell.lblCodeFlight.text = "\(data1.flightDesignator.carrierCode)-\(data1.flightDesignator.flightNumber)"
                cell.lblPrice.text = totPrice
                
                if data1.gdsCode == "QG"{
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight8")
                } else if data1.gdsCode == "ID" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight1")
                } else if data1.gdsCode == "GA" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight0")
                } else if data1.gdsCode == "IL" {
                   cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight2")
                } else if data1.gdsCode == "IN" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight3")
                } else if data1.gdsCode == "IW" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight4")
                } else if data1.gdsCode == "JT" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight5")
                } else if data1.gdsCode == "KD" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight6")
                } else if data1.gdsCode == "OD" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight7")
                } else if data1.gdsCode == "SJ" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight10")
                } else if data1.gdsCode == "SL" {
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight11")
                } else if data1.gdsCode == "QZ" || data1.gdsCode == "XT"{
                    cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight9")
                }
                
                return cell
                
            }else if indexPath.row == 1 {
                let data = self.data.flightSegments[0]
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceDetailTableViewCell.identifier, for: indexPath) as! PriceDetailTableViewCell
                    
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                let adult = numberFormatter.string(from: NSNumber(value:data.fares[0].basicFare))
                cell.lblPriceAdult.text = "IDR \(adult!)"
                cell.lblAdultFare.text = "Adult Fare (x\(noOfAdt))"
                var totalVAT : Int = 0
                totalVAT = data.fares[0].basicVat
                    
                if noOfChd == "0"{
                    cell.viewChild.isHidden = true
                    cell.heightViewChild.constant = 0
                    cell.topViewInfant.constant = cell.topViewChild.constant
                }else {
                    let child = numberFormatter.string(from: NSNumber(value:data.fares[0].childFare))
                    cell.viewChild.isHidden = false
                    cell.lblChildFare.text = "Child Fare (x\(noOfChd))"
                    cell.lblPriceChild.text = "IDR \(child!)"
                    totalVAT = totalVAT + data.fares[0].childVat
                }
                    
                if noOfInf == "0"{
                    cell.viewInfant.isHidden = true
                    cell.heightViewInfant.constant = 0
                    cell.topViewVAT.constant = cell.topViewInfant.constant
                }else{
                    let infant = numberFormatter.string(from: NSNumber(value:data.fares[0].childFare))
                    cell.viewInfant.isHidden = false
                    cell.lblInfantFare.text = "Infant Fare (x\(noOfInf))"
                    cell.lblPriceInfant.text = "IDR \(infant!)"
                    totalVAT = totalVAT + data.fares[0].childVat + data.fares[0].infantVat
                }
                    
                if noOfChd == "0" && noOfInf == "0"{
                    cell.viewChild.isHidden = true
                    cell.viewInfant.isHidden = true
                    cell.heightViewChild.constant = 0
                    cell.heightViewInfant.constant = 0
                    cell.topViewVAT.constant = cell.topViewChild.constant
                }
                let VAT = numberFormatter.string(from: NSNumber(value:totalVAT))
                cell.lblVAT.text = "IDR \(VAT!)"
                let tot = data.fares[0].airportTax + data.fares[0].iwjr
                cell.lblService.text = numberFormatter.string(from: NSNumber(value:tot))
                cell.lblTotalPay.text = totPrice
                
                return cell
                
        
            }else if indexPath.row == 2 {
                let data = self.data
                return FlightSummaryTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
            }
            
            return UITableViewCell()
        
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: PassengerDetailTableViewCell.identifier, for: indexPath) as! PassengerDetailTableViewCell
            
            let rowAdult = Int(noOfAdt)!
            let rowChild = Int(noOfAdt)! + Int(noOfChd)!
            
            var fName = ""
            if passengersColl[safe: indexPath.row] != nil {
                fName = passengersColl[indexPath.row].fName
            }
            
            switch indexPath.row {
            case 0...rowAdult - 1:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Adult)" : "Passenger (Adult)"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            case rowAdult...rowChild - 1:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Child)" : "Passenger (Child)"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            default:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Infant)" : "Passenger (Infant)"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            }
           
            /*cell.lblName.text = "\(titlePassenger!). \(namePassenger!) (ADT)"
            cell.lblName.font = UIFont(name: "Nunito-Bold", size: 14)!*/
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        let label = UILabel(frame: CGRect(x: 20, y: 2, width: tableView.frame.size.width, height: 35))
        label.font = UIFont(name: "Nunito-SemiBold", size: 14)!
        label.text = titleSection[section]
        label.textColor = UIColor.black
        view.addSubview(label)
        view.backgroundColor = UIColor(hexString: "E6E6E6")
    
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


extension ReviewBookingViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0{
                return 73
                
            }else if indexPath.row == 1{
                return 193
                
            }else if indexPath.row == 2{
                return 263
            }
        }else if indexPath.section == 1 {
            return 44
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
}

extension ReviewBookingViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}
