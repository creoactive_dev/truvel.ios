//
//  MenuBarViewController.swift
//  Truvel
//
//  Created by Calista on 10/11/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
protocol menuViewDelegate: class {
    func onDismiss(controller:MenuBarViewController)
   }

class MenuBarViewController: UIViewController {
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = MenuBarTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: MenuBarTableViewCell.identifier)
            
            table.delegate = self
            table.dataSource = self
        }
    }
    var delegate : menuViewDelegate?
    let titles = ["Subscibe to Newsletter","Help","Term of Use","Settings"]
    @IBAction func closeMe(_ sender: Any) {
        if let del = delegate {
            del.onDismiss(controller: self)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}

extension MenuBarViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuBarTableViewCell.identifier, for: indexPath) as! MenuBarTableViewCell
        cell.lblTitle.text = titles[indexPath.row]
        return cell
      
    }
    
}

extension MenuBarViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}
