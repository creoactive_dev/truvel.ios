//
//  SuccessPaymentViewController.swift
//  Truvel
//
//  Created by Calista on 12/10/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class SuccessPaymentViewController: UIViewController {
    @IBOutlet weak var lblTransactionID: UILabel!
    @IBOutlet weak var lblTransactionMsg: UILabel!
    @IBOutlet weak var btnHome: UIButton!

    var transactionID = ""
    var message = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnHome.layer.cornerRadius = 8
        lblTransactionID.text = "TVL02\(transactionID)"
        lblTransactionMsg.text = "\(message)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func home(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.home) as! HomeViewController
        let nc = UINavigationController.init(rootViewController: vc)
        
        self.present(nc, animated: true, completion: nil)
    }
    

}
