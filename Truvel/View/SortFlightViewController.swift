//
//  SortFlightViewController.swift
//  Truvel
//
//  Created by Calista on 10/18/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

enum FlightSort: String {
    case descending
    case ascending
    case lowest
    case highest
}

protocol sortViewDelegate: class {
    func onCanceled(controller:SortFlightViewController)
    func onDidSelect(controller:SortFlightViewController, type: FlightSort)
}
class SortFlightViewController: UIViewController {
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = SortTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: SortTableViewCell.identifier)
            
            table.dataSource = self
            table.delegate = self
        }
    }

    var delegate : sortViewDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

       btnCancel.layer.cornerRadius = 8
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func cancel(_ sender: Any) {
        delegate?.onCanceled(controller: self)
        
    }
    
}

extension SortFlightViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = ""
        return SortTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            delegate?.onDidSelect(controller: self, type: .lowest)
        
        }else if indexPath.row == 1{
            delegate?.onDidSelect(controller: self, type: .descending)
        
        }else if indexPath.row == 2{
            delegate?.onDidSelect(controller: self, type: .descending)
        
        }else if indexPath.row == 3{
            delegate?.onDidSelect(controller: self, type: .descending)
        }
    }
    
}

extension SortFlightViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 46
        
    }
}
