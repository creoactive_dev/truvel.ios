//
//  CalenderViewController.swift
//  Truvel
//
//  Created by Calista on 10/13/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import JTAppleCalendar

protocol calenderViewDelegate: class {
    func onDismissed(controller:CalenderViewController)
    func onOkay(controller: CalenderViewController, date: Date)
}

class CalenderViewController: UIViewController {
    @IBOutlet weak var calenderCollectionView: JTAppleCalendarView! {
        didSet {
            calenderCollectionView.calendarDataSource = self
            calenderCollectionView.calendarDelegate = self
            
            calenderCollectionView.minimumInteritemSpacing = 0
            calenderCollectionView.minimumLineSpacing = 0
            
            calenderCollectionView.allowsMultipleSelection = true
            calenderCollectionView.isRangeSelectionUsed = true
            
            calenderCollectionView.visibleDates { (visibleDates) in
                self.setupViewsOfCalendar(from: visibleDates)
            }
           // calenderCollectionView.cellSize = 46
        }
    }
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblDateSelected: UILabel!

    var delegate : calenderViewDelegate?
    var startDate: Date?
    var endDate: Date?
    var dateSelected: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let start = startDate, let end = endDate {
            calenderCollectionView.selectDates(from: start, to: end, triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
        }
        
        let today = Date()
        
        let form = DateFormatter()
        form.dateFormat = "dd"
        let dates = form.string(from: today)
        
        let format = DateFormatter()
        format.dateFormat = "EE"
        let day = format.string(from: today)
        
        let formt = DateFormatter()
        formt.dateFormat = "MMM"
        let month = formt.string(from: today)
        lblDateSelected.text = "\(day), \(month) \(dates)"
        
    }

    internal func setupViewsOfCalendar(from visibleDates:  DateSegmentInfo) {
        guard let date = visibleDates.monthDates.first?.date else { return }
        /*let today = Date()*/
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM"
        
        let formatt = DateFormatter()
        formatt.dateFormat = "yyyy"
        let year = formatt.string(from: date)

        let currentMonth = formatter.string(from: date)
    
        lblMonth.text = "\(currentMonth) \(year)"
    }
    
    internal func handleDateSelection(cellView: JTAppleCell?, cellState: CellState) {
        guard let cell = cellView as? TruvelCalendarCell else { return }
        
        if cellState.isSelected {
           // if cellState.date == startDate {
            cell.viewSelected.isHidden = false
            cell.lblDay.bringSubview(toFront: cell.viewSelected)
            cell.viewSelected.layer.cornerRadius = cell.viewSelected.frame.width / 2
            
            print(cellState.date)
            dateSelected = cellState.date
            cell.lblDay.textColor = UIColor.white
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd"
            let dates = formatter.string(from: cellState.date)
            
            let form = DateFormatter()
            form.dateFormat = "MMM"
            let month = form.string(from: cellState.date)
            
            let format = DateFormatter()
            format.dateFormat = "EE"
            let day = format.string(from: cellState.date)
            
            let formatt = DateFormatter()
            formatt.dateFormat = "yyyy"
            lblYear.text = formatt.string(from: cellState.date)
            
            if let d = Int(dates){
                if d < 9 {
                    let dat = dates.replacingOccurrences(of: "0", with: "")
                    lblDateSelected.text = "\(day), \(month) \(dat)"
                    
                }else{
                    lblDateSelected.text = "\(day), \(month) \(dates)"
                }
            }
            
            
//            } else if cellState.date == endDate {
//                cell.backgroundColor = UIColor(red: 212 / 255, green: 246 / 255, blue: 194 / 255, alpha: 1)
//            } else {
//                cell.backgroundColor = UIColor(red: 212 / 255, green: 246 / 255, blue: 194 / 255, alpha: 1)
//            }
        } else {
            
//            if let start = startDate, let end = endDate {
//                if cellState.date > start && cellState.date < end {
//                    startDate = cellState.date
//                    endDate = nil
//                    
//                    calenderCollectionView.deselectAllDates(triggerSelectionDelegate: false)
//                    
//                    calenderCollectionView.selectDates(from: cellState.date, to: cellState.date, triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: false)
//                    return
//                }
//            }
            
            if cellState.date == startDate {
                startDate = nil
                endDate = nil
                
                calenderCollectionView.deselectAllDates(triggerSelectionDelegate: false)
            }
            
//            if cellState.date == endDate {
//                startDate = cellState.date
//                endDate = nil
//                
//                calenderCollectionView.deselectAllDates(triggerSelectionDelegate: false)
//                
//                calenderCollectionView.selectDates(from: cellState.date, to: cellState.date, triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: false)
//            }
        
            if cellState.dateBelongsTo == .thisMonth {
                cell.lblDay.isHidden = false
                cell.backgroundColor = .white
                cell.lblDay.textColor = .black
            } else {
              //  cell.backgroundColor = UIColor(red: 240 / 255, green: 244 / 255, blue: 247 / 255, alpha: 1)
                cell.lblDay.isHidden = true
            }
            
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        delegate?.onDismissed(controller: self)
    }
    
    @IBAction func oke(_ sender: Any) {
        if let date = dateSelected{
            delegate?.onOkay(controller: self, date: date)
        }
    }
}

extension CalenderViewController: JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        let startDate = Date()
        let endDate = Calendar.current.date(byAdding: Calendar.Component.year, value: 3, to: Date())!
        
        let parameter = ConfigurationParameters(startDate: startDate, endDate: endDate)
        return parameter
    }
    
}

extension CalenderViewController: JTAppleCalendarViewDelegate {
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "TruvelCalendarCell".lowercased(), for: indexPath) as! TruvelCalendarCell
        cell.viewSelected.isHidden = true
        cell.viewSelected.layer.cornerRadius = cell.viewSelected.frame.width / 2
        cell.lblDay.text = cellState.text
        
        let today = Date()
        let formt = DateFormatter()
        formt.dateFormat = "dd"
        let now = formt.string(from: today)
        
        if cellState.text == now{
           
        }
        
        handleDateSelection(cellView: cell, cellState: cellState)
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        if self.startDate != nil {
            if date < self.startDate! {
                
                calendar.deselectAllDates(triggerSelectionDelegate: false)
                
                self.endDate = nil
                self.startDate = date
                
                calendar.selectDates(from: date, to: date, triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
            } else {
                self.endDate = date
            }
        } else {
            self.startDate = date
        }
        
        if let start = self.startDate, let end = self.endDate {
            calendar.selectDates(from: start, to: end, triggerSelectionDelegate: false, keepSelectionIfMultiSelectionAllowed: false)
        }
        
        handleDateSelection(cellView: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
}

class TruvelCalendarCell: JTAppleCell {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var viewSelected: UIView!
}
