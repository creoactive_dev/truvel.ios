//
//  ResultFlightViewController.swift
//  Truvel
//
//  Created by Calista on 10/14/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class ResultFlightViewController: UIViewController {
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = ScheduleTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: ScheduleTableViewCell.identifier)

            table.delegate = self
            table.dataSource = self
        }
    }
    @IBOutlet weak var lblFlight: UILabel!
    var depart = ""
    var arrival = ""
    var departureDate = ""
    var returnDate = ""
    var noOfAdt = ""
    var noOfChd = ""
    var noOfInf = ""
    var sortPrice = ""
    var priceMin = ""
    var priceMax = ""
    var cityFrom = ""
    var cityTo = ""
    
    let flightCode:[String] = ["GA","IL","QZ","KD","QG","SJ"]
    
    @IBOutlet weak var lblPackage: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var lblPleaseWait: UILabel!
    @IBOutlet weak var heightLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var indicator: DGActivityIndicatorView!
    
    /*internal var flightItem = [FlightJourneys](){
        didSet{
            table.reloadData()
        }
    }*/
    
    var flightItem: [FlightJourneys] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.isHidden = true
        lblFlight.text = "\(depart), \(cityFrom) - \(arrival), \(cityTo)"
        indicator.type = DGActivityIndicatorAnimationType.ballPulse
        indicator.tintColor = UIColor(hexString: "00A49E")
        indicator.startAnimating()
        lblPleaseWait.isHidden = false
//        progressView.animate(duration: 60)
        progressView.isHidden = false
        progressView.setProgress(0, animated: true)
        
        let adult = Int(noOfAdt)
        let child = Int(noOfChd)
        let infant = Int(noOfInf)
        print("total \(String(describing: adult)) \(String(describing: child)) \(String(describing: infant))")
        let total = adult! + child! + infant!
        
        let deFormatter = DateFormatter()
        deFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let departs = deFormatter.date(from: departureDate)
        let form = DateFormatter()
        form.dateFormat = "dd MMM yyyy"
        let date = form.string(from: departs!)
        
        let format = DateFormatter()
        format.dateFormat = "EE"
        let day = format.string(from: departs!)
        
        self.lblPackage.text = "\(day), \(date) - \(total) pax"
        
        self.getAirAvailable(index: 0)
      //  self.setFilter()
        

    }
    //MARK: - Function
    func getAirAvailable(index: Int) {
        if index > self.flightCode.count - 1 {
            self.progressView.isHidden = true
            let sortedArray = self.flightItem.sorted {
                $0.flightSegments[0].fares[0].basicFare < $1.flightSegments[0].fares[0].basicFare
            }
            
            self.flightItem = sortedArray
            self.table.reloadData()
            
            return
        }
        
        CheckAirportFlight().getAirAvailability(depart: depart, arrival: arrival, departureDate: departureDate, returnDate: returnDate, flightCode: self.flightCode[index], noOfAdt: noOfAdt, noOfChd: noOfChd, noOfInf: noOfInf, onSuccess: {
                [weak self] (code, message, result) in
                guard let res = result else {
                    return
                }
            
                if self?.flightItem.count == 0 {
                    self?.flightItem = res[0].flightJourneys
                } else {
                    self?.flightItem.append(contentsOf: res[0].flightJourneys)
                }
            
               // print("airport \(res[0].flightJourneys[0].arrivalAirport.city)")
                self?.lblPleaseWait.isHidden = true
                self?.heightLabelConstraint.constant = 0
                self?.indicator.stopAnimating()
                self?.table.isHidden = false
            
                let ratio = Float(index+1) / Float((self?.flightCode.count)!)
                self?.progressView.setProgress(ratio, animated: true)
            
                print(message)
                print("Do action when data available")
            }, onFailed: {
                (message) in
                print(message)
                print("Do action when data failed to fetching here")
                
                self.getAirAvailable(index: index+1)
            }, onComplete: {
                (message) in
                print(message)
                print("Do action when data complete fetching here")
                
                self.table.reloadData()
                
                self.getAirAvailable(index: index+1)
        })
    }
    
    func setFilter(){
        CheckAirportFlight().postFilter(sortPrice: "descending", priceMin: "0", priceMax: "1000000", stop: "all", onSuccess: { (code, message, result) in
            
        }, onFailed: { (message) in
            
        }, onComplete: {(message) in
        
        })
    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func menuBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func sort(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.sort) as! SortFlightViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func filter(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.filter) as! FilterFlightViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func refine(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.calender) as! CalenderViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
}

extension ResultFlightViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("count \(flightItem.count)")
        return flightItem.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleTableViewCell.identifier, for: indexPath) as! ScheduleTableViewCell
        cell.selectionStyle = .none
        let data = flightItem[indexPath.row].flightSegments[0]
        cell.lblCodeArrival.text = data.arrivalAirport.cityCode
        cell.lblCodeDeparture.text = data.departureAirport.cityCode
        cell.lblAirlines.text = data.airline.name
        cell.lblTime.text = "\(data.duration.hours)h \(data.duration.minutes)m"
        cell.lblCodeFlight.text = "\(data.flightDesignator.carrierCode)-\(data.flightDesignator.flightNumber)"
        
        let deFormatter = DateFormatter()
        deFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let startTime = deFormatter.date(from: data.std)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: startTime!)
        
        let dtFormatter1 = DateFormatter()
        dtFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let startTime1 = dtFormatter1.date(from: data.sta)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "HH:mm"
        let timeString1 = formatter1.string(from: startTime1!)
        
        cell.lblDepartureTime.text = timeString
        cell.lblArrivalTime.text = timeString1
        
        if flightItem[indexPath.row].stopCount == 1 {
            let data1 = flightItem[indexPath.row].flightSegments[0]
            let data2 = flightItem[indexPath.row].flightSegments[1]
            cell.lblCodeDeparture.text = data1.departureAirport.cityCode
            cell.lblCodeTransit1.text = "\(data1.arrivalAirport.cityCode)"
            cell.lblCodeArrival.text = data2.arrivalAirport.cityCode
            
            cell.imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-1-transit")
        } else if flightItem[indexPath.row].stopCount == 2 {
            let data1 = flightItem[indexPath.row].flightSegments[0]
            let data2 = flightItem[indexPath.row].flightSegments[1]
            let data3 = flightItem[indexPath.row].flightSegments[2]
            cell.lblCodeDeparture.text = data1.departureAirport.cityCode
            cell.lblCodeTransit1.text = "\(data1.arrivalAirport.cityCode) \(data2.arrivalAirport.cityCode)"
            cell.lblCodeArrival.text = data3.arrivalAirport.cityCode
            
            cell.imgDirect.image = #imageLiteral(resourceName: "ico-flight-line2-transit")
        } else{
            cell.imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-direct")
        }
        
        var adult:Int = 0
        var child:Int = 0
        var infant:Int = 0
        
        if let ad = Int(noOfAdt) {
            adult = ad
        }
        
        if let ch = Int(noOfChd){
            child = ch
        }
        
        if let inf = Int(noOfInf){
            infant = inf
        }
        
        let adultfare = data.fares[0].basicFare + data.fares[0].basicVat + data.fares[0].iwjr + data.fares[0].fuelSurcharge + data.fares[0].airportTax + data.fares[0].otherFee + data.fares[0].adminFee
        let adultTotal = adult * adultfare
        
        let childFare = data.fares[0].childFare + data.fares[0].childVat + data.fares[0].iwjr + data.fares[0].fuelSurcharge + data.fares[0].airportTax + data.fares[0].otherFee + data.fares[0].adminFee
        let childTotal = child * childFare
        
        let infantFare = data.fares[0].infantFare + data.fares[0].infantVat + data.fares[0].iwjr
        let infantTotal = infant * infantFare
        let total = adultTotal + childTotal + infantTotal
//        print("total \(total)")
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:total))
        
        if let price = formattedNumber {
            cell.lblPrice.text = "IDR \(price)"
        }
   
        
        if data.gdsCode == "QG"{
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight8")
        } else if data.gdsCode == "ID" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight1")
        } else if data.gdsCode == "GA" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight0")
        } else if data.gdsCode == "IL" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight2")
        } else if data.gdsCode == "IN" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight3")
        } else if data.gdsCode == "IW" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight4")
        } else if data.gdsCode == "JT" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight5")
        } else if data.gdsCode == "KD" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight6")
        } else if data.gdsCode == "OD" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight7")
        } else if data.gdsCode == "SJ" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight10")
        } else if data.gdsCode == "SL" {
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight11")
        } else if data.gdsCode == "QZ" || data.gdsCode == "XT"{
            cell.imgAirlines.image = #imageLiteral(resourceName: "ico-flight9")
        }
    
        return cell
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.summary) as! FlightSummaryViewController
        vc.data = flightItem[indexPath.row]
        vc.noOfAdt = self.noOfAdt
        vc.noOfChd = self.noOfChd
        vc.noOfInf = self.noOfInf
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ResultFlightViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
}

extension ResultFlightViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension ResultFlightViewController: sortViewDelegate{
    func onCanceled(controller: SortFlightViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onDidSelect(controller: SortFlightViewController, type: FlightSort) {
         self.sortPrice = type.rawValue
        print("type \(type.rawValue)")
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
       
    }
}

extension ResultFlightViewController: filterViewDelegate{
    func onApply(controller: FilterFlightViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onReset(controller: FilterFlightViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension ResultFlightViewController: calenderViewDelegate{
    func onDismissed(controller: CalenderViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onOkay(controller: CalenderViewController, date: Date) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        
    }
}
