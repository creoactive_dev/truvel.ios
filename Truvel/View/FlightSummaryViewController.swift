//
//  FlightSummaryViewController.swift
//  Truvel
//
//  Created by Calista on 10/18/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class FlightSummaryViewController: UIViewController {
    @IBOutlet weak var btnBook: UIButton!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = FlightSummaryTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: FlightSummaryTableViewCell.identifier)
            
            let xib1 = PriceDetailTableViewCell.nib
            table.register(xib1, forCellReuseIdentifier: PriceDetailTableViewCell.identifier)
            
            table.dataSource = self
            table.delegate = self

        }
    }
    @IBOutlet weak var lblAirlines: UILabel!
    @IBOutlet weak var lblJourney: UILabel!
    @IBOutlet weak var imgFlight: UIImageView!
    @IBOutlet weak var lblFlight: UILabel!
    @IBOutlet weak var lblCodeFlight: UILabel!
    @IBOutlet weak var lblPriceUp: UILabel!
    @IBOutlet weak var lblPriceDown: UILabel!
    @IBOutlet weak var imgDirect: UIImageView!
    @IBOutlet weak var lblCodeDepart: UILabel!
    @IBOutlet weak var lblCodeDirect: UILabel!
    @IBOutlet weak var lblCodeArrival: UILabel!
    @IBOutlet weak var viewNettPrice: UIView!
    
    var data : FlightJourneys! = nil
    var isOpenDetailPrice = false
    var noOfAdt = ""
    var noOfChd = ""
    var noOfInf = ""
    var isChild = false
    var isInfant = false
    var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnBook.layer.cornerRadius = 8
        viewNettPrice.layer.cornerRadius = 3
        isOpenDetailPrice = false
        self.setData()
    }

    func setData() {
        let data1 = data.flightSegments[0]
        lblAirlines.text = data1.airline.name
        lblJourney.text = "\(data1.departureAirport.city)-\(data1.arrivalAirport.city) | \(data1.duration.hours)h \(data1.duration.minutes)m Direct"
        lblFlight.text = data1.airline.name
        lblCodeFlight.text = "\(data1.flightDesignator.carrierCode)-\(data1.flightDesignator.flightNumber)"
        
        var adult:Int = 0
        var child:Int = 0
        var infant:Int = 0
        
        if let ad = Int(noOfAdt) {
            adult = ad
        }
        
        if let ch = Int(noOfChd){
            child = ch
        }
        
        if let inf = Int(noOfInf){
            infant = inf
        }
        
        if noOfChd == "0"{
            isChild = false
        }else{
            isChild = true
        }
        
        if noOfInf == "0"{
            isInfant = false
        }else{
            isInfant = true
        }
        
        let adultfare = data1.fares[0].basicFare + data1.fares[0].basicVat + data1.fares[0].iwjr + data1.fares[0].fuelSurcharge + data1.fares[0].airportTax + data1.fares[0].otherFee + data1.fares[0].adminFee
        let adultTotal = adult * adultfare
        
        let childFare = data1.fares[0].childFare + data1.fares[0].childVat + data1.fares[0].iwjr + data1.fares[0].fuelSurcharge + data1.fares[0].airportTax + data1.fares[0].otherFee + data1.fares[0].adminFee
        let childTotal = child * childFare
        
        let infantFare = data1.fares[0].infantFare + data1.fares[0].infantVat + data1.fares[0].iwjr
        let infantTotal = infant * infantFare
        self.total = adultTotal + childTotal + infantTotal
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:self.total))
        
        if let price = formattedNumber{
            lblPriceUp.text = "IDR \(price)"
            lblPriceDown.text = "IDR \(price)"
        }
        
        if data.stopCount == 0 {
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-direct")
            lblCodeDirect.isHidden = true
            
        }else if data.stopCount == 1{
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line-1-transit")
            lblCodeDirect.isHidden = false
            lblCodeDirect.text = "TRANSIT"
        }else {
            imgDirect.image = #imageLiteral(resourceName: "ico-flight-line2-transit")
        }
        
        lblCodeDepart.text = data.departureAirport.city
        lblCodeArrival.text = data.arrivalAirport.city
        
        if data1.gdsCode == "QG"{
            imgFlight.image = #imageLiteral(resourceName: "ico-flight8")
        } else if data1.gdsCode == "ID" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight1")
        } else if data1.gdsCode == "GA" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight0")
        } else if data1.gdsCode == "IL" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight2")
        } else if data1.gdsCode == "IN" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight3")
        } else if data1.gdsCode == "IW" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight4")
        } else if data1.gdsCode == "JT" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight5")
        } else if data1.gdsCode == "KD" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight6")
        } else if data1.gdsCode == "OD" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight7")
        } else if data1.gdsCode == "SJ" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight10")
        } else if data1.gdsCode == "SL" {
            imgFlight.image = #imageLiteral(resourceName: "ico-flight11")
        } else if data1.gdsCode == "QZ" || data1.gdsCode == "XT"{
            imgFlight.image = #imageLiteral(resourceName: "ico-flight9")
        }
    }
  
    //MARK:- Action
    @IBAction func bookThisFlight(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.contactInfo) as! ContactInformationViewController
        vc.price = lblPriceUp.text
        vc.data = data
        vc.noOfAdt = noOfAdt
        vc.noOfChd = noOfChd
        vc.amount = total
        vc.noOfInf = noOfInf
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func priceDetail(_ sender: Any) {
        isOpenDetailPrice = true
        self.table.reloadData()
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func menuBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
}

extension FlightSummaryViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension FlightSummaryViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        view1.backgroundColor = UIColor.clear
        self.view.addSubview(view1)
        
        return view1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if isOpenDetailPrice{
                let data = self.data.flightSegments[0]
                let cell = tableView.dequeueReusableCell(withIdentifier: PriceDetailTableViewCell.identifier, for: indexPath) as! PriceDetailTableViewCell
                
                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = NumberFormatter.Style.decimal
                let adult = numberFormatter.string(from: NSNumber(value:data.fares[0].basicFare))
                cell.lblPriceAdult.text = "IDR \(adult!)"
                cell.lblAdultFare.text = "Adult Fare (x\(noOfAdt))"
                var totalVAT : Int = 0
                totalVAT = data.fares[0].basicVat
                
                if noOfChd == "0"{
                    cell.viewChild.isHidden = true
                    cell.heightViewChild.constant = 0
                    cell.topViewInfant.constant = cell.topViewChild.constant
                }else {
                    let child = numberFormatter.string(from: NSNumber(value:data.fares[0].childFare))
                    cell.viewChild.isHidden = false
                    cell.lblChildFare.text = "Child Fare (x\(noOfChd))"
                    cell.lblPriceChild.text = "IDR \(child!)"
                    totalVAT = totalVAT + data.fares[0].childVat
                }
                
                if noOfInf == "0"{
                    cell.viewInfant.isHidden = true
                    cell.heightViewInfant.constant = 0
                    cell.topViewVAT.constant = cell.topViewInfant.constant
                }else{
                    let infant = numberFormatter.string(from: NSNumber(value:data.fares[0].childFare))
                    cell.viewInfant.isHidden = false
                    cell.lblInfantFare.text = "Infant Fare (x\(noOfInf))"
                    cell.lblPriceInfant.text = "IDR \(infant!)"
                    totalVAT = totalVAT + data.fares[0].childVat + data.fares[0].infantVat
                }
                
                if noOfChd == "0" && noOfInf == "0"{
                    cell.viewChild.isHidden = true
                    cell.viewInfant.isHidden = true
                    cell.heightViewChild.constant = 0
                    cell.heightViewInfant.constant = 0
                    cell.topViewVAT.constant = cell.topViewChild.constant
                }
                let VAT = numberFormatter.string(from: NSNumber(value:totalVAT))
                cell.lblVAT.text = "IDR \(VAT!)"
                let tot = data.fares[0].airportTax + data.fares[0].iwjr
                cell.lblService.text = numberFormatter.string(from: NSNumber(value:tot))
                cell.lblTotalPay.text = lblPriceUp.text
                
                return cell
            }else{
                 return UITableViewCell()
            }
           
            
        }else if indexPath.row == 1 {
            let data = self.data
            return FlightSummaryTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
          
            
        }
        
        return UITableViewCell()
    }
    
}

extension FlightSummaryViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            if isOpenDetailPrice{
                if noOfChd == "0" || noOfInf == "0"{
                    return 173
                    
                }else if noOfChd == "0" && noOfInf == "0"{
                    return 153
                }
                return 193
            }
                return 0
        
        }else if indexPath.row == 1 {
            return 263
            
        }else{
            return 0
        }
        
    }
}
