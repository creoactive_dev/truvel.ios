//
//  SearchFlightViewController.swift
//  Truvel
//
//  Created by Calista on 10/11/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class SearchFlightViewController: UIViewController {
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var switchReturn: UISwitch!
    @IBOutlet weak var imgCalenderReturn: UIImageView!
    @IBOutlet weak var lblDateReturn: UILabel!
    @IBOutlet weak var lblMonthReturn: UILabel!
    @IBOutlet weak var lblDayReturn: UILabel!
    @IBOutlet weak var btnCalenderReturn: UIButton!
    @IBOutlet weak var lblDateDepart: UILabel!
    @IBOutlet weak var lblMonthDepart: UILabel!
    @IBOutlet weak var lblDayDepart: UILabel!
    
    @IBOutlet weak var lblAdult: UILabel!
    @IBOutlet weak var lblChild: UILabel!
    @IBOutlet weak var lblInfant: UILabel!
    @IBOutlet weak var lblCodeFlyingFrom: UILabel!
    @IBOutlet weak var lblNameFlyingFrom: UILabel!
    @IBOutlet weak var lblCodeFlyingTo: UILabel!
    @IBOutlet weak var lblNameFlyingTo: UILabel!
    @IBOutlet weak var btnSwitchFlight: UIButton!
    
    let datePicker = UIDatePicker()
    let toolbar = UIToolbar()
    
    var isSwitch = false
    var codeFlyingFrom : String?
    var codeFlyingTo : String?
    var nameFlyingFrom : String?
    var nameFlyingTo : String?
    var isDepart = false
    var departTime: String?
    var returnTime: String?
    
    var adultCount: String?
    var childCount: String?
    var infantCount: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true

        self.btnSearch.layer.cornerRadius = 8
        switchReturn.transform = CGAffineTransform(scaleX: 0.75, y: 0.75);
        switchReturn.backgroundColor = UIColor(hexString:"B3B3B3")
        switchReturn.layer.cornerRadius = 16
        switchReturn.tintColor = UIColor(hexString:"B3B3B3")
        btnCalenderReturn.isHidden = true
        
        isSwitch = false
        
        if codeFlyingTo == nil {
            codeFlyingTo = lblCodeFlyingTo.text
            nameFlyingTo = lblNameFlyingTo.text
            codeFlyingFrom = lblCodeFlyingFrom.text
            nameFlyingFrom = lblNameFlyingFrom.text
        } else {
            codeFlyingFrom = lblCodeFlyingFrom.text
            nameFlyingFrom = lblNameFlyingFrom.text
            
            lblCodeFlyingTo.text = codeFlyingTo
            lblNameFlyingTo.text = nameFlyingTo
            lblCodeFlyingFrom.text = codeFlyingFrom
            lblNameFlyingFrom.text = nameFlyingFrom
        }
        
         let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        let dates = formatter.string(from: date)
        
        let form = DateFormatter()
        form.dateFormat = "MMM yyyy"
        let month = form.string(from: date)
        
        let format = DateFormatter()
        format.dateFormat = "EEEE"
        let day = format.string(from: date)
        
        let formats = DateFormatter()
        formats.dateFormat = "yyyy-MM-dd'T'00:00:00"
        departTime = formats.string(from: date)
        
        lblDateDepart.text = dates
        lblMonthDepart.text = month
        lblDayDepart.text = day
        lblDateReturn.text = dates
        lblMonthReturn.text = month
        lblDayReturn.text = day
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }

    @IBAction func menuBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    @IBAction func switchFlight(_ sender: Any) {
        if isSwitch {
            //keganti
            lblCodeFlyingTo.text = codeFlyingFrom
            lblNameFlyingTo.text = nameFlyingFrom
            lblCodeFlyingFrom.text = codeFlyingTo
            lblNameFlyingFrom.text = nameFlyingTo
            isSwitch = false
        }else {
            //normal
            lblCodeFlyingTo.text = codeFlyingTo
            lblNameFlyingTo.text = nameFlyingTo
            lblCodeFlyingFrom.text = codeFlyingFrom
            lblNameFlyingFrom.text = nameFlyingFrom
            isSwitch = true
        }
    }
    
    
    @IBAction func returnFlight(_ sender: Any) {
        if switchReturn.isOn {
            imgCalenderReturn.image = #imageLiteral(resourceName: "ico-calendar-active")
            lblDateReturn.textColor = UIColor.black
            lblDayReturn.textColor = UIColor.black
            lblMonthReturn.textColor = UIColor.black
            btnCalenderReturn.isHidden = false
            
        } else{
            imgCalenderReturn.image = #imageLiteral(resourceName: "ico-calendar-idle")
            lblDateReturn.textColor = UIColor.lightGray
            lblDayReturn.textColor = UIColor.lightGray
            lblMonthReturn.textColor = UIColor.lightGray
            btnCalenderReturn.isHidden = true
        }
    }

    @IBAction func flyingFrom(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.flying) as! FlyingSearchViewController
        vc.delegate = self
        vc.isFlyingFrom = true
       self.present(vc, animated: true, completion: nil)

    }
    
    @IBAction func flyingTo(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.flying) as! FlyingSearchViewController
        vc.delegate = self
        vc.isFlyingFrom = false
        self.present(vc, animated: true, completion: nil)
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.frame = CGRect.init(x: 0, y: self.view.frame.height - 240, width: self.view.frame.width, height: 240)
        datePicker.backgroundColor = UIColor.white
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 1, to: Date())
        
        //ToolBar
        toolbar.frame = CGRect.init(x: 0, y: self.view.frame.height - (240+30), width: self.view.frame.width, height: 30)
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(SearchFlightViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(SearchFlightViewController.cancelDatePicker))
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        self.view.addSubview(datePicker)
        self.view.addSubview(toolbar)
    }
    
    func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        let dates = formatter.string(from: datePicker.date)
        
        let form = DateFormatter()
        form.dateFormat = "MMM yyyy"
        let month = form.string(from: datePicker.date)
        
        let format = DateFormatter()
        format.dateFormat = "EEEE"
        let day = format.string(from: datePicker.date)
        
        lblDateDepart.text = dates
        lblMonthDepart.text = month
        lblDayDepart.text = day
        
        let formats = DateFormatter()
        formats.dateFormat = "yyyy-MM-dd'T'00:00:00"
        departTime = formats.string(from: datePicker.date)
        
        datePicker.removeFromSuperview()
        toolbar.removeFromSuperview()
    }
    
    func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        datePicker.removeFromSuperview()
        toolbar.removeFromSuperview()
    }
    
    @IBAction func calenderDepart(_ sender: Any) {
        /*isDepart = true
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.calender) as! CalenderViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)*/
        showDatePicker()
    }
    
    @IBAction func calenderReturn(_ sender: Any) {
        isDepart = false
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.calender) as! CalenderViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)

    }
    
    @IBAction func changePassenger(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.passenger) as! ChangePassengerViewController
        vc.delegate = self
        vc.adultCount = lblAdult.text
        vc.childCount = lblChild.text
        vc.infantCount = lblInfant.text
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)

    }
    
    @IBAction func search(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.result) as! ResultFlightViewController
        vc.depart = self.lblCodeFlyingFrom.text!
        vc.arrival = self.lblCodeFlyingTo.text!
        vc.cityTo = self.lblNameFlyingTo.text!
        vc.cityFrom = self.lblNameFlyingFrom.text!
        vc.noOfAdt = self.lblAdult.text!
        vc.noOfChd = self.lblChild.text!
        vc.noOfInf = self.lblInfant.text!
        
        if let departT = departTime{
            vc.departureDate = departT
        }
        
        if let returnT = returnTime{
            if returnTime == nil{
                vc.returnDate = ""
            }else{
                vc.returnDate = returnT
            }
            
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension SearchFlightViewController: flightViewDelegate{
    func onFlyingFrom(controller: FlyingSearchViewController, code: String, city: String) {
       controller.dismiss(animated: true, completion: nil)
        codeFlyingFrom = code
        nameFlyingFrom = city
        self.lblCodeFlyingFrom.text = code
        self.lblNameFlyingFrom.text = city
    }
    
    func onFlyingTo(controller: FlyingSearchViewController, code: String, city: String) {
        controller.dismiss(animated: true, completion: nil)
        codeFlyingTo = code
        nameFlyingTo = city
        self.lblCodeFlyingTo.text = code
        self.lblNameFlyingTo.text = city
    }
    
}

extension SearchFlightViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension SearchFlightViewController: calenderViewDelegate{
    func onDismissed(controller: CalenderViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onOkay(controller: CalenderViewController, date: Date) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        let dates = formatter.string(from: date)
        
        let form = DateFormatter()
        form.dateFormat = "MMM yyyy"
        let month = form.string(from: date)
        
        let format = DateFormatter()
        format.dateFormat = "EEEE"
        let day = format.string(from: date)
        
        if isDepart{
            lblDateDepart.text = dates
            lblMonthDepart.text = month
            lblDayDepart.text = day
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd'T'00:00:00"
            departTime = format.string(from: date)
            
        }else{
            lblDateReturn.text = dates
            lblMonthReturn.text = month
            lblDayReturn.text = day
            let format = DateFormatter()
            format.dateFormat = "yyyy-MM-dd'T'00:00:00"
            returnTime = format.string(from: date)
        }
        
        
    }
}

extension SearchFlightViewController: passengerViewDelegate{
    func onCancel(controller: ChangePassengerViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
    
    func onSelect(controller: ChangePassengerViewController, adult: String, child: String, infant: String) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
        self.lblAdult.text = adult
        self.lblChild.text = child
        self.lblInfant.text = infant
    }
}

