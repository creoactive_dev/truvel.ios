//
//  ContactDetailViewController.swift
//  Truvel
//
//  Created by Calista on 9/24/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

protocol contactDetailDelegate: class {
    func didSelectPassenger(passenger:Passengers, index: Int)
}
class ContactDetailViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var table: UITableView!{
        didSet{
            table.register(BirthdateTableViewCell.self, forCellReuseIdentifier: "cell")
            table.delegate = self
            table.dataSource = self
        }
    }
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var txtPhoneNo: UITextField!
    @IBOutlet weak var lblNationality: UILabel!
    @IBOutlet weak var pickerGender: UIPickerView!{
        didSet{
            pickerGender.dataSource = self
            pickerGender.delegate = self
        }
    }
    
    var isPassenger = false
    var countryCodeArr: String?
    var countryCodeDepart: String?
    var fullname: String?
    var phoneNo: String?
    var titleGender = ["Mr","Ms"]
    var titleG : String?
    var fare: Fares?
    var delegate: contactDetailDelegate?
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        pickerGender.isHidden = true
    
      btnSubmit.layer.cornerRadius = 8
        if fullname != nil || fullname != "" {
            txtFullname.text = fullname
        }
        
        if phoneNo != nil || phoneNo != "" {
            txtPhoneNo.text = phoneNo
        }
        
        if titleG == nil {
            lblTitle.text = "Mr."
        }else{
            lblTitle.text = titleG
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: -Function
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }

    
    
    //MARK: -Action
    @IBAction func back(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }

    @IBAction func openMenuRight(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)

    }
    
    @IBAction func changeTitle(_ sender: Any) {
        pickerGender.isHidden = false
    }
    
    @IBAction func changeCountryCode(_ sender: Any) {
        
    }
  
    @IBAction func submit(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
        /* INI CONTOH DOANG LOH ngirim saat submit di view yg mau dirombak */
         guard let fare = fare else { return }
        var gender = ""
        if (lblTitle.text?.contains("Mr"))! {
            gender = "male"
        } else {
            gender = "female"
        }
        
         let senger = Passengers(passNum: 0, idNo: "", paxType: "ADT", fName: txtFullname.text!, mName: "", lName: "Wijaya", title: (lblTitle.text?.replacingOccurrences(of: ".", with: ""))!, dob: "", gender: gender, nationality: "Indonesia", ticketNum: "", paxFare: fare, infant: "kosong")
         delegate?.didSelectPassenger(passenger: senger, index: index)
    }
    
}

extension ContactDetailViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        return cell
    }
   
    
}

extension ContactDetailViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if countryCodeDepart == "ID" && countryCodeArr == "ID"{
            return 0
        } else{
            return 77
        }
      
    }
}

extension ContactDetailViewController: UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titleGender.count
    }
}

extension ContactDetailViewController: UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titleGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerGender.isHidden = true
        self.lblTitle.text = "\(titleGender[row])."
        //table.reloadData()
    }
    
}


extension ContactDetailViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}
