//
//  ChangePassengerViewController.swift
//  Truvel
//
//  Created by Calista on 10/13/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
protocol passengerViewDelegate: class {
    func onCancel(controller:ChangePassengerViewController)
    func onSelect(controller: ChangePassengerViewController, adult: String, child: String, infant: String)
}


class ChangePassengerViewController: UIViewController {
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblAdult: UILabel!
    @IBOutlet weak var lblChild: UILabel!
    @IBOutlet weak var lblInfant: UILabel!
    @IBOutlet weak var btnPlusAdult: UIButton!
    @IBOutlet weak var btnPlusChild: UIButton!
    @IBOutlet weak var btnPlusInfant: UIButton!

    var delegate : passengerViewDelegate?
    var total = 0
    var plus = 1
    
    
    var adultCount: String?
    var childCount: String?
    var infantCount: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("\(adultCount) \(childCount) \(infantCount)")

        btnCancel.layer.cornerRadius = 10
        btnSelect.layer.cornerRadius = 10
        
        if let ac = adultCount {
            lblAdult.text = ac
        } else {
            lblAdult.text = "0"
        }
        
        if let cc = childCount {
            lblChild.text = cc
        } else {
            lblChild.text = "0"
        }
        
        if let ic = infantCount {
            lblInfant.text = ic
        } else {
            lblInfant.text = "0"
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func minAdult(_ sender: Any) {
        let min = Int(lblAdult.text!)! - 1
        let infant = Int(lblInfant.text!)!
        
        if min <= 1 {
            lblAdult.text = "1"
            total = total - 1
        }else {
            lblAdult.text = String(min)
            total = total - min
        }
        
        if infant > Int(lblAdult.text!)! {
            let minInfant = infant - 1
            lblInfant.text = String(minInfant)
        }
        
        self.btnPlusAdult.isEnabled = true
    }
    
    @IBAction func minChild(_ sender: Any) {
        let min = Int(lblChild.text!)! - 1
        if min <= 0 {
            lblChild.text = "0"
            total = total - 0
        }else {
            lblChild.text = String(min)
            total = total - min
        }
       
    }
    
    @IBAction func minInfant(_ sender: Any) {
        let min = Int(lblInfant.text!)! - 1
        if min <= 0 {
            lblInfant.text = "0"
            total = total - 0
        }else {
            lblInfant.text = String(min)
            total = total - min
        }
    }
    
    @IBAction func maxAdult(_ sender: Any) {
        let adult = Int(lblAdult.text!)!
        let max = Int(lblAdult.text!)! + plus
        let child = Int(lblChild.text!)!

        total = max + child
        print("total adult\(total)")
        
        if total > 7 {
            plus = 0
        }else if total < 7 {
            plus = 1
        }
        
        if total > 7 {
            self.btnPlusChild.isEnabled = false
            let alert = JDropDownAlert(position: .bottom)
            alert.alertWith("The number of adult and child passengers must not exceed 7", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: #imageLiteral(resourceName: "Flight_Module_Mockup"))
            self.btnPlusChild.isEnabled = true
            
            return
        }
        
        if adult < 7 {
            lblAdult.text = String(max)
            
        }else if lblAdult.text == "7"{
            self.btnPlusAdult.isEnabled = false
            let alert = JDropDownAlert(position: .bottom)
            alert.alertWith("The number of adult and child passengers must not exceed 7", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: #imageLiteral(resourceName: "Flight_Module_Mockup"))
        }
    
        /*if total == 7 {
            self.btnPlusAdult.isEnabled = false
            let alert = JDropDownAlert(position: .bottom)
            alert.alertWith("The number of adult and child passengers must not exceed 7", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: #imageLiteral(resourceName: "Flight_Module_Mockup"))
        }*/

        
        self.btnPlusAdult.isEnabled = true
        

    }
    
    @IBAction func maxChild(_ sender: Any) {
        let max = Int(lblChild.text!)! + plus
        let child = Int(lblChild.text!)!
        let adult = Int(lblAdult.text!)!
        
        total = max + adult
        
        if total > 7 {
            plus = 0
        }else if total < 7 {
            plus = 1
        }
        print("total child \(total)")
        
        if total > 7 {
            self.btnPlusChild.isEnabled = false
            let alert = JDropDownAlert(position: .bottom)
            alert.alertWith("The number of adult and child passengers must not exceed 7", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: #imageLiteral(resourceName: "Flight_Module_Mockup"))
            self.btnPlusChild.isEnabled = true
            
            return
        }
        
        if child < 6 {
            lblChild.text = String(max)
        } else if lblChild.text == "6"{
            self.btnPlusChild.isEnabled = false
        }
        
        self.btnPlusChild.isEnabled = true


    }
    
    @IBAction func maxInfant(_ sender: Any) {
        let infant = Int(lblInfant.text!)!
        let max = Int(lblInfant.text!)! + plus
        let adult = Int(lblAdult.text!)!

        total = max
        print("total infant \(total)")
        
        if total == 7 {
            plus = 0
        }else if total < 7 {
            plus = 1
        }
        
        if total > adult {
            self.btnPlusInfant.isEnabled = false
            let alert = JDropDownAlert(position: .bottom)
            alert.alertWith("The number of infant must not more than adult", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: #imageLiteral(resourceName: "Flight_Module_Mockup"))
            
            self.btnPlusInfant.isEnabled = true
            
            return
        }
        
        if infant < 4 {
            lblInfant.text = String(max)
        } else if lblInfant.text == "4" {
            self.btnPlusInfant.isEnabled = false
        }
        
        self.btnPlusInfant.isEnabled = true
        
    }
    
    
    @IBAction func cancel(_ sender: Any) {
        if let del = delegate {
            del.onCancel(controller: self)
        }

    }

   
    @IBAction func selectPassenger(_ sender: Any) {
        var adultCount: String = "0"
        var childCount: String = "0"
        var infantCount: String = "0"
        
        if let ac = lblAdult.text {
            adultCount = ac
        }
        
        if let cc = lblChild.text {
            childCount = cc
        }
        
        if let ic = lblInfant.text {
            infantCount = ic
        }
        
        delegate?.onSelect(controller: self, adult: adultCount, child: childCount, infant: infantCount)

    }
    
}
