//
//  FlyingSearchViewController.swift
//  Truvel
//
//  Created by Calista on 10/13/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
protocol flightViewDelegate: class {
    func onFlyingFrom(controller: FlyingSearchViewController, code: String, city: String)
    func onFlyingTo(controller: FlyingSearchViewController, code: String, city: String)
}

class FlyingSearchViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = SearchFlightTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: SearchFlightTableViewCell.identifier)
            
            table.delegate = self
            table.dataSource = self
        }
    }
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var country: [String] = [""]
    var airports = [ListAirport]() {
        didSet {
            temps = airports
            table.reloadData()
        }
    }
    
    var temps :[ListAirport] = []
    var delegate: flightViewDelegate?
    var isFlyingFrom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        country = ["INDONESIA"]
        spinner.startAnimating()
        setupData()
        // Do any additional setup after loading the view.
    }

    private func setupData() {
        ListAirportController().getListAirport(
            onSuccess: {
                [weak self] (code, message, result) in
                guard let strongSelf = self else { return }
                guard let res = result else { return }
                
                strongSelf.airports = res
                self?.spinner.stopAnimating()
                self?.spinner.isHidden = true
                print(message)
                print("Do action when data available")
            }, onFailed: {
                [weak self] (message) in
              //  guard let strongSelf = self else { return }
              //  strongSelf.viewErrorHandler?.show()
                print(message)
                print("Do action when data failed to fetching here")
            }, onComplete: {
                [weak self] (message) in
                //guard let strongSelf = self else { return }
                //strongSelf.viewErrorHandler?.hide()
                print(message)
                print("Do action when data complete fetching here")
        })
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkSearch(txt: String) {
        temps = []
        
        for airport in airports {
            if airport.city.lowercased().contains(txt) || airport.code.lowercased().contains(txt) || airport.cityCode.lowercased().contains(txt) || airport.name.lowercased().contains(txt) {
              temps.append(airport)
                print("ada")
            }
        }
        print("temp \(temps)")
        table.reloadData()
    }
    
    @IBAction func searchDidChange(_ sender: Any) {
        print("didchange \(txtSearch.text!)")
        checkSearch(txt: txtSearch.text!.lowercased())
        
    }
    
    @IBAction func closeMe(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FlyingSearchViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if txtSearch.text == ""{
            return airports.count
        }else{
            return temps.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if txtSearch.text == "" {
            let data = self.airports[indexPath.row]
            return SearchFlightTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
        }else{
            let data = self.temps[indexPath.row]
            return SearchFlightTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
//        let label = UILabel(frame: CGRect(x: 20, y: 2, width: tableView.frame.size.width, height: 35))
//        label.font = UIFont(name: "Nunito-SemiBold", size: 14)!
//        label.text = country[section]
//        label.textColor = UIColor.black
//        view.addSubview(label)
//        view.backgroundColor = UIColor(hexString: "E6E6E6")
//            
//        return view
//    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.temps[indexPath.row]
        if isFlyingFrom{
             delegate?.onFlyingFrom(controller: self, code: data.code, city: data.name)
        }else{
            delegate?.onFlyingTo(controller: self, code: data.code, city: data.name)
        }
       
        
    }
    
}

extension FlyingSearchViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 35
//    }
}

extension FlyingSearchViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
}

