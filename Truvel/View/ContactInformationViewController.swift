//
//  ContactInformationViewController.swift
//  Truvel
//
//  Created by Calista on 10/14/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class ContactInformationViewController: UIViewController {
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var viewNettPrice: UIView!
    @IBOutlet weak var btnContBooking: UIButton!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = ContactDetailTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: ContactDetailTableViewCell.identifier)
            
            let xib1 = PassengerDetailTableViewCell.nib
            table.register(xib1, forCellReuseIdentifier: PassengerDetailTableViewCell.identifier)

            table.dataSource = self
            table.delegate = self
        }
    }
    @IBOutlet weak var pickerGender: UIPickerView!{
        didSet{
            pickerGender.dataSource = self
            pickerGender.delegate = self
        }
    }
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgTransit: UIImageView!
    @IBOutlet weak var lblCodeDepart: UILabel!
    @IBOutlet weak var lblCodeTransit1: UILabel!
    @IBOutlet weak var lblCodeArrival: UILabel!
    
    var titleSection : [String] = [""]
    var titleGender = ["Mr","Ms"]
    var titleSelected: String?
    var price: String?
    var amount: Int?
    var data : FlightJourneys! = nil
    var isPassenger = false
    var namePassenger: String?
    var phoneNo: String?
    var noOfAdt = ""
    var noOfChd = ""
    var noOfInf = ""
    var total = 0
    var contacts: Contacts! = nil
    var passengersColl: [Passengers] = [Passengers]()
    var email: String?
    var passengerDidSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleSection = ["CONTACT DETAILS", "PASSENGER DETAILS"]
        isPassenger = false
        titleSelected = "Mr"
        setDisplay()
    }
    
    func setDisplay() {
        btnPayment.titleLabel?.lineBreakMode = .byWordWrapping
        btnPayment.titleLabel?.numberOfLines = 0
        viewNettPrice.layer.cornerRadius = 3
        btnContBooking.layer.cornerRadius = 5
        pickerGender.isHidden = true
        lblPrice.text = price
        let data1 = data.flightSegments[0]
        lblCodeDepart.text = data1.departureAirport.cityCode
        lblCodeArrival.text = data1.arrivalAirport.cityCode
        if data.stopCount == 0 {
            lblCodeTransit1.isHidden = true
            imgTransit.image = #imageLiteral(resourceName: "ico-flight-line-direct")
            
        }else if data.stopCount == 1 {
            lblCodeTransit1.isHidden = false
            lblCodeTransit1.text = data.flightSegments[1].departureAirport.cityCode
            imgTransit.image = #imageLiteral(resourceName: "ico-flight-line-1-transit")
            
        }else{
            lblCodeTransit1.isHidden = false
            lblCodeTransit1.text = data1.arrivalAirport.cityCode
            imgTransit.image = #imageLiteral(resourceName: "ico-flight-line2-transit")
        }

        let adult = Int(noOfAdt)
        let child = Int(noOfChd)
        let infant = Int(noOfInf)
        total = adult! + child! + infant!
    }
    
    func processBook(controller : WarningViewController ) {
      //  if isPassenger{
            FlightBookingController().getBookFlight(fname: namePassenger!, mName: "", lName: namePassenger!, title: self.titleSelected!, mphone: phoneNo!, email: email!, city: "", passenger: passengersColl, paxFare:data.flightSegments[0].fares , infant: "", flightJourney: data, onSuccess: { (code, message, result) in
                
                controller.indicator.stopAnimating()
                controller.willMove(toParentViewController: nil)
                controller.view.removeFromSuperview()
                
                let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.review) as! ReviewBookingViewController
                vc.transactionID = result //stg  711110011179 //"001110011177" prod
                vc.data = self.data
                vc.total = self.total
                vc.noOfAdt = self.noOfAdt
                vc.noOfChd = self.noOfChd
                vc.noOfInf = self.noOfInf
                vc.amount = self.amount
                vc.totPrice = self.lblPrice.text
                vc.titlePassenger = self.titleSelected
                vc.namePassenger = self.namePassenger
                vc.phoneNo = self.phoneNo
                vc.email = self.email
                vc.passengersColl = self.passengersColl
               self.navigationController?.pushViewController(vc, animated: true)
            }, onFailed: { (message) in
                
            }, onComplete: {(message) in
                
            })
      /*  }
        else{
            FlightBookingController().getBookFlight(fname: namePassenger!, mName: "", lName: namePassenger!, title: self.titleSelected!, mphone: phoneNo!, email: email!, city: "", passenger: passengersColl, paxFare:data.flightSegments[0].fares , infant: "", flightJourney: data, onSuccess: { (code, message, result) in
                print("result \(String(describing: result))")
                
                controller.indicator.stopAnimating()
                controller.willMove(toParentViewController: nil)
                controller.view.removeFromSuperview()
                 
            }, onFailed: { (message) in
                
            }, onComplete: {(message) in
                
            })
        }*/
        
    }
    
    //MARK: -Action
    @IBAction func continueBooking(_ sender: Any) {
        if namePassenger == nil || phoneNo == nil || email == nil {
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Please fill your contact detail", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: nil)
            
            return
        }
        
        if passengersColl.count < total {
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Please fill your pessanger detail", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: nil)
            
            return
        }
        
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.warning) as! WarningViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
        
    }
    
    @IBAction func openRightBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)

    }

    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func openTitle(){
        pickerGender.isHidden = false
    }
    
    func selectPassenger(){
        if namePassenger == nil || namePassenger == "" {
            let alert = JDropDownAlert(position: .top)
            alert.alertWith("Please fill your name", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: nil)
        } else{
            isPassenger = true
            
            var gender = ""
            if (titleSelected?.contains("Mr"))! {
                gender = "male"
            }else {
                gender = "female"
            }
            
            let senger = Passengers(passNum: 0, idNo: "", paxType: "ADT", fName: namePassenger!, mName: "", lName: "Wijaya", title: (titleSelected!.replacingOccurrences(of: ".", with: "")), dob: "", gender: gender, nationality: "Indonesia", ticketNum: "", paxFare: data.flightSegments[0].fares[0], infant: "kosong")
            passengersColl.append(senger)
        }
        
        table.reloadData()
    }
 }

extension ContactInformationViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return titleSection.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }else{
            return total
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        view1.backgroundColor = UIColor.clear
        self.view.addSubview(view1)
        
        return view1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        let label = UILabel(frame: CGRect(x: 20, y: 2, width: tableView.frame.size.width, height: 25))
        label.font = UIFont(name: "Nunito-SemiBold", size: 12)!
        label.text = titleSection[section]
        label.textColor = UIColor.black
        view.addSubview(label)
        view.backgroundColor = UIColor(hexString: "E6E6E6")
        
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ContactDetailTableViewCell.identifier, for: indexPath) as! ContactDetailTableViewCell
            cell.delegate = self
            cell.btnTitle.addTarget(self, action: #selector(openTitle), for: UIControlEvents.touchUpInside)
           
            cell.lblTitle.text = "\(titleSelected!)."

            cell.btnPassenger.addTarget(self, action: #selector(selectPassenger), for: UIControlEvents.touchUpInside)
            
            return cell
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: PassengerDetailTableViewCell.identifier, for: indexPath) as! PassengerDetailTableViewCell
            
            let rowAdult = Int(noOfAdt)!
            let rowChild = Int(noOfAdt)! + Int(noOfChd)!
            
            var fName = ""
            if passengersColl[safe: indexPath.row] != nil {
                fName = passengersColl[indexPath.row].fName
            }
            
            switch indexPath.row {
            case 0...rowAdult - 1:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Adult)" : "Passenger (Adult)"
                cell.btnFindDetail.text = fName.characters.count > 0 ? "Edit" : "FIND IN DETAILS"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            case rowAdult...rowChild - 1:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Child)" : "Passenger (Child)"
                cell.btnFindDetail.text = fName.characters.count > 0 ? "Edit" : "FIND IN DETAILS"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            default:
                cell.lblName.text = fName.characters.count > 0 ? "\(fName) (Infant)" : "Passenger (Infant)"
                cell.btnFindDetail.text = fName.characters.count > 0 ? "Edit" : "FIND IN DETAILS"
                cell.lblName.font = fName.characters.count > 0 ? UIFont(name: "Nunito-Bold", size: 14)! : UIFont(name: "Nunito-Regular", size: 14)!
            }
            
            return cell
            
            
        }
        
        return UITableViewCell()
    }
    
}

extension ContactInformationViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1{
            return 25
        }else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 284
            
        }else if indexPath.section == 1 {
            return 44
            
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if indexPath.row > 0 {
                if passengersColl[safe: indexPath.row - 1] == nil {
                    let alert = JDropDownAlert(position: .top)
                    alert.alertWith("Input should not be random, must be ordered based on input", message: nil, topLabelColor: UIColor.white, messageLabelColor: UIColor.white, backgroundColor: UIColor(hexString:"E32D40"), image: nil)
                    
                    return
                }
            }
            
            let data1 = data.flightSegments[0]
            let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.contactDetail) as! ContactDetailViewController
            vc.delegate = self
            vc.isPassenger = isPassenger
            vc.fare = data1.fares[0]
            vc.countryCodeArr = data1.arrivalAirport.countryCode
            vc.countryCodeDepart = data1.departureAirport.countryCode
            vc.index = indexPath.row
            
            if passengersColl[safe: indexPath.row] != nil {
                vc.fullname = passengersColl[indexPath.row].fName
                vc.phoneNo = phoneNo
                vc.titleG = titleSelected
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension ContactInformationViewController: UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return titleGender.count
    }
}

extension ContactInformationViewController: UIPickerViewDataSource{
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return titleGender[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerGender.isHidden = true
        titleSelected = titleGender[row]
        table.reloadData()
    }
    
}

extension ContactInformationViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension ContactInformationViewController: warningViewDelegate{
    func onContinue(controller: WarningViewController) {
        controller.viewWarning.isHidden = true
        controller.viewWaiting.isHidden = false
        controller.indicator.type = DGActivityIndicatorAnimationType.ballClipRotate
        controller.indicator.tintColor = UIColor.white //UIColor(hexString: "00A49E")
        
        controller.indicator.startAnimating()
        
        processBook(controller: controller)
    }
    
    func onRecheck(controller: WarningViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension ContactInformationViewController: contactDelegate{
    func fullnameDidChange(name: String, title: String) {
        namePassenger = name
        titleSelected = title
    }
    
    func phoneNoDidChange(no: String) {
        phoneNo = no
    }
    
    func emailDidChange(email: String) {
        self.email = email
    }
    
}

extension ContactInformationViewController: contactDetailDelegate{
    func didSelectPassenger(passenger: Passengers, index: Int) {
        if passengersColl[safe: index] != nil {
            passengersColl[index] = passenger
        } else {
            passengersColl.append(passenger)
        }
        
        self.table.reloadData()
    }
}

extension Collection where Indices.Iterator.Element == Index {
    
    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Generator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
