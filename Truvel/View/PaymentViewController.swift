//
//  PaymentViewController.swift
//  Truvel
//
//  Created by Calista on 11/28/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import SwiftyJSON
import DGActivityIndicatorView

class PaymentViewController: UIViewController {
    @IBOutlet weak var btnPayment: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = DepartureTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: DepartureTableViewCell.identifier)
            
            let xib1 = PaymentMethodTableViewCell.nib
            table.register(xib1, forCellReuseIdentifier: PaymentMethodTableViewCell.identifier)

            
            table.delegate = self
            table.dataSource = self
        }
        
    }
    
    @IBOutlet weak var lblTruvelBooking: UILabel!
    var titleSection: [String] = [""]
    var data : FlightJourneys! = nil
    var totPrice : String?
    var amount: Int?
    var transactionID: String?
    var invoiceNo: String?
    var phoneNo : String?
    
    var titlePassenger: String?
    var namePassenger: String?
    var email: String?
    
    var indexDoku: Int?
    
    //MARK: variable Doku
    var MerchantSharedKey = "4TWmL47vUz1e"
    var MerchantSharedMallID = "4273"
    var MerchantPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn2kuDI3QkuJ61rQQLAHI+gvaDSfd+ScY+OjkoGtpOe2EQZkyGv3+0nF1zQpK9lu4jB+ru2WWDjgEdXf0xeqomfsbc08LsbKXEmNQyaDD0Fpe+H2PeUwlxywru+kLgsKuAsSx/7+TBENRC53Qt99ZrCWjsIJuPq1bpjuqcMlbAr5SI1D5hgSXledR2y1/QZUdXPuGN3JgsExcomMNf29LZPnSKk9L4pdX8t0RGgYS7ITRzXH4BjDt16TL6X/KsCcjV3ZyOxZmNK/OttKqz/eq+bBAUnYRCONbHXOZS6AyZJiLnvExQrCV/PY6ETEdaOSAeyEozsTFjURFY9wC3o1uwQIDAQAB"
    //#define DokuPayTokenPayment @"9133428f7966395ee657e0a51c1c92dcd9bcd52f"
    //var DokuPayCustomerID = "69800071"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnPayment.titleLabel?.lineBreakMode = .byWordWrapping
        btnPayment.titleLabel?.numberOfLines = 0
        titleSection = ["DEPARTURE FLIGHT", "SELECT PAYMENT METHOD"]
        let data1 = data.flightSegments[0]
        lblTime.text = "\(data1.duration.hours)H \(data1.duration.minutes)M \(data1.duration.seconds)S"
        lblTruvelBooking.text = "Truvel Booking Code TVL\(transactionID!)"
        
    }
    
    //MARK: -Function
    func getPaymentItem()->DKPaymentItem  {
        let paymentItem = DKPaymentItem()
        let basket = [["name": invoiceNo, //invoice_no
                       "amount": totPrice,
                        "quantity": "1",
                        "subtotal": totPrice]]
        
        let ammount = totPrice?.replacingOccurrences(of: "IDR ", with: "").replacingOccurrences(of: ",", with: "")
        print("ammount \(ammount!)")
        paymentItem.dataAmount = "\(ammount!).00" //"15000"
        paymentItem.dataBasket = basket
        let uuid = UIDevice.current.identifierForVendor?.uuidString
        paymentItem.dataImei = (uuid?.replacingOccurrences(of: "-", with: ""))!
        paymentItem.dataCurrency = "360"
        paymentItem.dataMerchantChain = "NA"
        paymentItem.dataSessionID = "sha1(date(YmdHis))"
        paymentItem.dataTransactionID = invoiceNo! //"051110011175"
        paymentItem.isProduction = false
        paymentItem.dataMerchantCode = MerchantSharedMallID
        paymentItem.publicKey = MerchantPublicKey
        paymentItem.sharedKey = MerchantSharedKey
        paymentItem.dataWords = paymentItem.generateWords()
        paymentItem.mobilePhone = phoneNo! //"081573002188"
  
        return paymentItem
    }
    
    
    //MARK: -Action
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
 
    @IBAction func menuBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
    }
    
    func processPayment(controller: WarningViewController, dict: [String: Any]) {
        controller.viewWarning.isHidden = true
        controller.viewWaiting.isHidden = false
        controller.indicator.type = DGActivityIndicatorAnimationType.ballClipRotate
        controller.indicator.tintColor = UIColor.white //UIColor(hexString: "00A49E")
        
        controller.indicator.startAnimating()
        
        FlightBookingController().getPaymentNotif(amount: self.amount!, currencyCode: "IDR", transactID: self.transactionID!, param: dict, onSuccess: { (status, message, result) in
            controller.indicator.stopAnimating()
            controller.willMove(toParentViewController: nil)
            controller.view.removeFromSuperview()
            
            let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.success) as! SuccessPaymentViewController
            vc.transactionID = self.transactionID!
            vc.message = result == "success" ? "is success. Please check your email for further info." : "is failed. Please contact us for further info."
            self.present(vc, animated: true, completion: nil)
        }, onFailed: { (message) in
            print(message)
        }, onComplete: { (message) in
            print(message)
        })
    }


}
extension PaymentViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {

            let data1 = self.data.flightSegments[0]
            let cell = tableView.dequeueReusableCell(withIdentifier: DepartureTableViewCell.identifier, for: indexPath) as! DepartureTableViewCell
            cell.selectionStyle = .none
            cell.lblAirlines.text = data1.airline.name
            cell.lblCodeFlight.text = "\(data1.flightDesignator.carrierCode)-\(data1.flightDesignator.flightNumber)"
            cell.lblPrice.text = totPrice
            
            if data1.gdsCode == "QG"{
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight8")
            } else if data1.gdsCode == "ID" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight1")
            } else if data1.gdsCode == "GA" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight0")
            } else if data1.gdsCode == "IL" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight2")
            } else if data1.gdsCode == "IN" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight3")
            } else if data1.gdsCode == "IW" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight4")
            } else if data1.gdsCode == "JT" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight5")
            } else if data1.gdsCode == "KD" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight6")
            } else if data1.gdsCode == "OD" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight7")
            } else if data1.gdsCode == "SJ" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight10")
            } else if data1.gdsCode == "SL" {
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight11")
            } else if data1.gdsCode == "QZ" || data1.gdsCode == "XT"{
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight9")
            }
                
            return cell
                
           
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: PaymentMethodTableViewCell.identifier, for: indexPath) as! PaymentMethodTableViewCell
            cell.selectionStyle = .none
            
            if indexPath.row == 0 {
                cell.lblMethod.text = "Virtual Account / ATM Transfer / Alfagroup"
                cell.imgMethod1.image = #imageLiteral(resourceName: "ico-payment-atm-bersama")
                cell.viewBottom.isHidden = false
                
            }else if indexPath.row == 1{
                cell.lblMethod.text = "Mandiri ClickPay"
                cell.imgMethod1.image = #imageLiteral(resourceName: "ico-payment-mandiri-clickpay")
                cell.viewBottom.isHidden = false
                
            }else if indexPath.row == 2{
                cell.lblMethod.text = "Credit Card"
                cell.imgMethod1.image = #imageLiteral(resourceName: "ico-payment-visa")
                cell.imgMethod2.image = #imageLiteral(resourceName: "ico-payment-mastercard")
                cell.viewBottom.isHidden = true
            }
           
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        let label = UILabel(frame: CGRect(x: 20, y: 2, width: tableView.frame.size.width, height: 35))
        label.font = UIFont(name: "Nunito-SemiBold", size: 14)!
        label.text = titleSection[section]
        label.textColor = UIColor.black
        view.addSubview(label)
        view.backgroundColor = UIColor(hexString: "E6E6E6")
        
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.indexDoku = 0
            
            let doku:DokuPay = DokuPay.sharedInstance() as! DokuPay
            doku.paymentItem = getPaymentItem()
            doku.paymentChannel = DokuPaymentChannelTypeVirtualAccount
            doku.delegate = self
            doku.presentPayment()
            
        }else if indexPath.row == 1 {
            self.indexDoku = 1
            
            let doku:DokuPay = DokuPay.sharedInstance() as! DokuPay
            doku.paymentItem = getPaymentItem()
            doku.paymentChannel = DokuPaymentChannelTypeMandiriClickPay
            doku.delegate = self
            doku.presentPayment()
            
        } else if indexPath.row == 2 {
            self.indexDoku = 2
            
            let doku:DokuPay = DokuPay.sharedInstance() as! DokuPay
            doku.paymentItem = getPaymentItem()
            doku.paymentChannel = DokuPaymentChannelTypeCC
            doku.delegate = self
            doku.presentPayment()
           
        }
    }
}


extension PaymentViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 73
            
        }else if indexPath.section == 1 {
            return 77
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
}

extension PaymentViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension PaymentViewController: DokuPayDelegate{
    func onDokuPaySuccess(_ dictData: [AnyHashable : Any]) {
        print("success doku \(dictData)")
        let jsonRespon = JSON(dictData).dictionaryValue
        let basket = [["name": invoiceNo,"amount": totPrice,"quantity": "1","subtotal": totPrice]]
        let customer = ["name":jsonRespon["res_name"]?.stringValue,"data_address":"Joglo","data_email":jsonRespon["res_data_email"]?.stringValue,"data_phone":jsonRespon["res_data_mobile_phone"]?.stringValue]
        let param = ["mall_id":MerchantSharedMallID,"sharedKey":MerchantSharedKey,"amount":jsonRespon["res_amount"]?.stringValue ?? "0.00","invoice_no":invoiceNo! ,"pairing_code":jsonRespon["res_pairing_code"]?.stringValue ?? "","token":jsonRespon["res_token_id"]?.stringValue ?? "","deviceid":jsonRespon["res_device_id"]?.stringValue ?? "","customer":customer,"basket":basket] as [String : Any]
        
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.warning) as! WarningViewController
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
        
        processPayment(controller: vc, dict: param)
    }
    
    func onDokuPayError(_ error: Error) {
         print("error doku \(error)")
    }
    
    func onDokuMandiriPaySuccess(_ dictData: [AnyHashable : Any]) {
        print("success doku mandiri \(dictData)")
        let ammount = totPrice?.replacingOccurrences(of: "IDR ", with: "").replacingOccurrences(of: ",", with: "")

        let jsonRespon = JSON(dictData).dictionaryValue
        let basket = [["name": invoiceNo!,"amount": totPrice!,"quantity": "1","subtotal": totPrice!]]
        let customer = ["name":namePassenger!,"data_address":"Joglo","data_email":email!,"data_phone":phoneNo!]
        let debitCard = jsonRespon["debitCard"]?.stringValue ?? ""
        let chal1 = jsonRespon["challenge1"]?.stringValue ?? ""
        let chal2 = jsonRespon["challenge2"]?.stringValue ?? ""
        let chal3 = jsonRespon["challenge3"]?.stringValue ?? ""
        let responVal = jsonRespon["responseValue"]?.stringValue ?? ""
        let param = ["mall_id":MerchantSharedMallID,"sharedKey":MerchantSharedKey,"amount":"\(ammount!).00","invoice_no":invoiceNo!,"customer":customer,"basket":basket,"card_no":debitCard,"CHALLENGE_CODE_1":chal1,"CHALLENGE_CODE_2":chal2,"CHALLENGE_CODE_3":chal3,"response_token":responVal] as [String : Any]
        
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.warning) as! WarningViewController
        
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)
        
        processPayment(controller: vc, dict: param)
    }
    
}
