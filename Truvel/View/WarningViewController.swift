//
//  WarningViewController.swift
//  Truvel
//
//  Created by Calista on 10/22/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

protocol warningViewDelegate: class {
    func onContinue(controller:WarningViewController)
    func onRecheck(controller: WarningViewController)
}


class WarningViewController: UIViewController {

    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var btnRecheck: UIButton!
    @IBOutlet weak var viewWarning: UIView!
    @IBOutlet weak var viewWaiting: UIView!
    @IBOutlet weak var indicator: DGActivityIndicatorView!
    
    var delegate : warningViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewWaiting.isHidden = true
        viewWarning.isHidden = false
        btnRecheck.layer.cornerRadius = 8
        btnContinue.layer.cornerRadius = 8
    }
    
    @IBAction func continued(_ sender: Any) {
        delegate?.onContinue(controller: self)
    }
  
    @IBAction func recheck(_ sender: Any) {
        delegate?.onRecheck(controller: self)
    }
}
