//
//  FilterFlightViewController.swift
//  Truvel
//
//  Created by Calista on 10/18/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

protocol filterViewDelegate: class {
    func onReset(controller:FilterFlightViewController)
    func onApply(controller: FilterFlightViewController)
}

class FilterFlightViewController: UIViewController {
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = FilterTransitTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: FilterTransitTableViewCell.identifier)
            
            let xib1 = FilterAirlinesTableViewCell.nib
            table.register(xib1, forCellReuseIdentifier: FilterAirlinesTableViewCell.identifier)
            
            table.dataSource = self
            table.delegate = self
        }
    }

    var delegate: filterViewDelegate?
    var filterCategory : [String] = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnReset.layer.cornerRadius = 8
        btnApply.layer.cornerRadius = 8
        
        filterCategory = ["TRANSIT", "AIRLINES", "DEPARTURE TIME"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func reset(_ sender: Any) {
        delegate?.onReset(controller: self)
    }
    
    @IBAction func apply(_ sender: Any) {
        delegate?.onApply(controller: self)
    }

}

extension FilterFlightViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterCategory.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
            
        }else if section == 1{
            return 11
        }
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 0))
        let label = UILabel(frame: CGRect(x: 20, y: 2, width: tableView.frame.size.width, height: 35))
        label.font = UIFont(name: "Nunito-SemiBold", size: 14)!
        label.text = filterCategory[section]
        label.textColor = UIColor.black
        view.addSubview(label)
        view.backgroundColor = UIColor(hexString: "E6E6E6")
        
        return view
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let data = ""
            return FilterTransitTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
            
        }else if indexPath.section == 1 {
            let data = ""
            return FilterAirlinesTableViewCell.configure(context: self, tableView: tableView, indexPath: indexPath, object: data)
            
        }
            return UITableViewCell()
    }
    
}

extension FilterFlightViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 60
            
        }else if indexPath.section == 1 {
            return 53
            
        }else{
            return 0
        }
        
    }
}
