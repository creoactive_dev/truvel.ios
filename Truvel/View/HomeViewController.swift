//
//  HomeViewController.swift
//  Truvel
//
//  Created by Calista on 9/23/17.
//  Copyright © 2017 Calista. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var table: UITableView!{
        didSet{
            let xib = SliderHomeTableViewCell.nib
            table.register(xib, forCellReuseIdentifier: SliderHomeTableViewCell.identifier)

            let xib2 = MenuHomeTableViewCell.nib
            table.register(xib2, forCellReuseIdentifier: MenuHomeTableViewCell.identifier)

            let xib3 = TopFlightTableViewCell.nib
            table.register(xib3, forCellReuseIdentifier: TopFlightTableViewCell.identifier)
            
            let xib4 = OurFlyingTableViewCell.nib
            table.register(xib4, forCellReuseIdentifier: OurFlyingTableViewCell.identifier)
          
            table.dataSource = self
            table.delegate = self
        }
    }
    
    var isHotel: Bool = false
    var isFlight: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func openRightBar(_ sender: Any) {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.main, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Main.menu) as! MenuBarViewController
        vc.delegate = self
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: vc)
        vc.view.frame = UIScreen.main.bounds
        vc.view.layoutIfNeeded()
        vc.view.backgroundColor = UIColor.clear
        self.view.addSubview(vc.view)
        self.view.bringSubview(toFront: vc.view)

    }
    
    func openSearch() {
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.search) as! SearchFlightViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    func openSearchFrmCollection(index: IndexPath) {
        let nameFly = ["Bali Ngurah Rai, Denpasar","Adi, Sutjipto, Jogja","Juanda, Surabaya","Kualanamu Intl, Medan","Lombok Intl, Lombok"]
        let codeFly = ["DPS","JOG","SUB","KNO","LOP"]
        
        let storyboard = UIStoryboard(name: REST.StoryboardReferences.flight, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: REST.ViewControllerID.Flight.search) as! SearchFlightViewController
        vc.codeFlyingTo = codeFly[index.row]
        vc.nameFlyingTo = nameFly[index.row]
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tabHotelSelected(){
        isHotel = true
        isFlight = false
        self.table.reloadData()
    }
    
    func tabFlightSelected() {
        isFlight = true
        isHotel = false
        self.table.reloadData()
    }
    
    
}

extension HomeViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 10))
        view1.backgroundColor = UIColor.clear
        self.view.addSubview(view1)
        
        return view1

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SliderHomeTableViewCell.identifier, for: indexPath) as! SliderHomeTableViewCell
            if isHotel {
                cell.txtSearch.placeholder = "Where do you want to stay?"
            } else if isFlight{
                cell.txtSearch.placeholder = "Where do you want to go?"
            }
            
            cell.context = self
            cell.btnSearch.addTarget(self, action: #selector(openSearch), for: .touchUpInside)
            return cell
            
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: MenuHomeTableViewCell.identifier, for: indexPath) as! MenuHomeTableViewCell
            
            /*cell.btnHotel.addTarget(self, action: #selector(tabHotelSelected), for: .touchUpInside)*/
            cell.btnFlight.addTarget(self, action: #selector(tabFlightSelected), for: .touchUpInside)
            if isHotel {
                cell.imgHotel.image = #imageLiteral(resourceName: "ico-hotel-bar-active-un")
                cell.lblHotel.textColor = UIColor(hexString: "FFCC00")
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight-bar-idle-un")
                cell.lblFlight.textColor = UIColor.darkGray
                cell.lineHotel.isHidden = false
                cell.lineFlight.isHidden = true
                
            }else if isFlight{
                cell.imgHotel.image = #imageLiteral(resourceName: "ico-hotel-bar-idle-un")
                cell.lblHotel.textColor = UIColor.darkGray
                cell.imgFlight.image = #imageLiteral(resourceName: "ico-flight-bar-active-un")
                cell.lblFlight.textColor = UIColor(hexString: "FFCC00")
                cell.lineHotel.isHidden = true
                cell.lineFlight.isHidden = false
            }
            
           return cell
            
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TopFlightTableViewCell.identifier, for: indexPath) as! TopFlightTableViewCell
            cell.delegate = self
            
            if isHotel{
                cell.lblTitleRow.text = "TOP CITIES DESTINATION"
                cell.tabFrom = 0
            }else if isFlight{
                cell.lblTitleRow.text = "TOP FLIGHT DESTINATION"
                 cell.tabFrom = 1
            }
            cell.context = self
            return cell
            
        }
        else if indexPath.row == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: OurFlyingTableViewCell.identifier, for: indexPath) as! OurFlyingTableViewCell
            cell.context = self
            if isHotel{
                cell.lblTitleRow.text = "OUR HOTEL GROUP PARTNER"
                cell.tabFrom = 0
            }else if isFlight{
                cell.lblTitleRow.text = "OUR FLYING PARTNER"
                cell.tabFrom = 1
            }

            return cell
        }
        
        return UITableViewCell()
    }
       
}

extension HomeViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 269

        }else if indexPath.row == 1 {
            return 75
            
        }else if indexPath.row == 2 {
            return 161
        
        }else if indexPath.row == 3 {
            return 200
        
        }else{
            return 0
        }
        
    }
}

extension HomeViewController: menuViewDelegate{
    func onDismiss(controller: MenuBarViewController) {
        controller.willMove(toParentViewController: nil)
        controller.view.removeFromSuperview()
    }
}

extension HomeViewController: ccollectionDelegate{
    func onDismiss(index: IndexPath) {
        openSearchFrmCollection(index: index)
    }
}


